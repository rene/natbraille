<?xml version="1.1" encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Blanchard, Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<!-- version 2.0 -->

<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">

<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'
xmlns:nat='http://natbraille.free.fr/xsl'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
<xsl:include href="fr-commun.xsl"/>

<xsl:variable name="voyelles" as="xs:string" select="'aeiouyàâéèêëîïôùûüæœ'"/>
<xsl:variable name="l_pt_sup" as="xs:string*" select="('&pt1;','&pt12;','&pt13;','&pt14;','&pt15;','&pt16;',
	'&pt123;','&pt124;','&pt125;','&pt126;','&pt1234;','&pt1235;','&pt1236;','&pt12345;','&pt12346;','&pt12356;','&pt1246;','&pt1256;','&pt12456;','&pt123456;',
	'&pt134;','&pt135;','&pt136;','&pt135;','&pt136;','&pt1345;','&pt1346;','&pt1356;','&pt13456;',
	'&pt14;','&pt15;','&pt16;','&pt145;','&pt146;','&pt1456;','&pt156;',
	'&pt345;','&pt24;','&pt3456;','&pt346;',
	'&pt4;','&pt45;','&pt46;','&pt456;',
	'a','e','i','o','u','y','à','â','é','è','ê','ë','î','ï','ô','ù','û','ü','æ','œ',
	'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z','ç')"/>

<xsl:variable name="l_ivb" as="xs:string" select="'â$|â[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ê$|ê[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|î$|î[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ô$|ô[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^û|û$|ë[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|ï[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^ï$|ü|&oelig;[bmp]|&oelig;$|w$|w[bçcdfghjklmnpqrstvwxz]|[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]w[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|k|q[^u]|q$|[^e]z$|^z$|ç$|ç[bçcdfghjklmnpqrstvwxz]|[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]è[aeiouyàâéèêëîïôùûüáíóúìòäö&aelig;&oelig;]|^ù$|^b$|^c$|^d$|^f$|^g$|^h$|^i$|^j$|^l$|^m$|^n$|^o$|^p$|^q$|^r$|^s$|^t$|^u$|^v$|^x$|^z$|^è$|^ù$|^ë$|^é$'"/>
<!-- pas besoin  car n'existe pas
	[^e]x[cons.]|^x[cons]|
	à$|
? => /?!"(*)'-§
-->


<xsl:variable name="l_amb_connues" as="xs:string*" select="('ient$', 'aient$')"/>

<xsl:template match="lit">
	<xsl:variable name="laPhrase" as="element()">
		<xsl:choose>
			<xsl:when test="local-name(parent::*) = 'titre' and (../@niveauOrig &lt; $minTitleAbr)">
<!-- 				<xsl:message select="'********NE PAS ABREGER*******'"/> -->
				<xsl:copy copy-namespaces="no">
					<xsl:attribute name="abreger" select="'false'"/>
					<xsl:copy-of copy-namespaces="no" select="@*|*|text()"/>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="phrMEF" as="element()">
					<xsl:copy copy-namespaces="no">
						<xsl:for-each select="*">
							<xsl:choose>
								<xsl:when test="local-name(.) = 'mot'">
									<xsl:variable as="element()*" name="decoupApos">
										<xsl:choose>
											<!-- gestion des apostrophes -->
											<xsl:when test="contains(.,'''') and not(number(translate(.,''',.','')) or index-of($motsTiret, lower-case(.))>0)">
												<xsl:variable name="splitApos" select="tokenize(.,'''')" as="xs:string*"/>
												<xsl:variable name="attr" select="@*"/>
												<xsl:for-each select="$splitApos">
													<xsl:element name="mot" inherit-namespaces="no">
														<xsl:copy-of select="$attr"/>
														<xsl:if test="not(position()=last())">
															<xsl:attribute name="doSpace" select="'false'"/>
														</xsl:if>
														<xsl:value-of select="if(position()=last()) then . else (concat(.,''''))"/>
													</xsl:element>
												</xsl:for-each>
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="." copy-namespaces="no"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<!-- gestion des tirets -->
									<xsl:for-each select="$decoupApos">
										<xsl:choose>
											<xsl:when test="contains(.,'-') and not(index-of($motsTiret, lower-case(.))>0)">
												<xsl:variable name="splitTiret" as="xs:string*" select="tokenize(.,'-')"/>
												<xsl:variable name="attr" select="./@*"/>
												<xsl:for-each select="$splitTiret">
													<xsl:element name="mot" inherit-namespaces="no">
														<xsl:copy-of select="$attr"/>
														<xsl:if test="not(position()=last())">
															<xsl:attribute name="doSpace" select="'false'"/>
														</xsl:if>
														<xsl:value-of select="."/>
													</xsl:element>
													<xsl:if test="not(position()=last())">
														<xsl:element name="mot" inherit-namespaces="no">
															<xsl:copy-of select="$attr"/>
															<xsl:attribute name="doSpace" select="'false'"/>
															<xsl:value-of select="'-'"/>
														</xsl:element>
													</xsl:if>
												</xsl:for-each>
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="."/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="." copy-namespaces="no"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:copy>
				</xsl:variable>
				<!--<xsl:message select="'PHRASE MEP:',$phrMEF"/>-->
				<xsl:copy copy-namespaces="no">
					<xsl:call-template name="litPass1Abr">
						<xsl:with-param name="noeud" select="$phrMEF/child::*[1]"/>
					</xsl:call-template>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!--<xsl:message select="'PHRASE AVANT PASS 2:',$laPhrase"/>-->
	<xsl:apply-templates select="$laPhrase" mode="pass2"/>
	<xsl:call-template name="espace"/>
</xsl:template>

<xsl:template match="lit" mode="pass2">
	<xsl:apply-templates select="*|text()|processing-instruction()" />
</xsl:template>

<xsl:template name="litPass1Abr">
	<xsl:param name="noeud" as="element()?"/>
	<xsl:variable name="noeudMin" select="fn:lower-case($noeud)"/>
	<xsl:variable name="noeudMaj" select="fn:upper-case($noeud)"/>
	<xsl:choose>
		<!-- noeud vide -->
		<xsl:when test="not($noeud/node())">
			<xsl:if test="$noeud/following-sibling::*[1]">
				<xsl:call-template name="litPass1Abr">
					<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1] "/>
				</xsl:call-template>
			</xsl:if>
		</xsl:when>
		<!-- dernier mot -->
		<xsl:when test="count($noeud/following-sibling::*)=0">
			<xsl:call-template name="litPass1AbrSignes">
				<xsl:with-param name="noeud" select="$noeud"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test="$noeud/self::mot and($noeud = replace(lower-case($noeud),'^.',substring($noeud,1,1)) or $noeud = fn:upper-case($noeud))">
			<xsl:variable name="suivant" select="$noeud/following-sibling::*[1]" as="element()?"/>
			<xsl:variable name="suivant2" select="$noeud/following-sibling::*[2]" as="element()?"/>
			<xsl:variable name="suivant3" select="$noeud/following-sibling::*[3]" as="element()?"/>
			<xsl:variable name="suivantMin" select="lower-case($suivant)"/>
			<xsl:variable name="suivantMaj" select="upper-case($suivant)"/>
			<xsl:variable name="suivant2Min" select="lower-case($suivant2)"/>
			<xsl:variable name="suivant2Maj" select="upper-case($suivant2)"/>
			<xsl:choose>
				<!-- locutions soit tout en majuscules soit tout en minuscule, soit avec une majuscule au début-->
				<xsl:when test="local-name($suivant) = 'mot' and($suivant = $suivantMin or $suivant = $suivantMaj)">
					<xsl:variable name="pos" as="xs:integer?" select="if($suivant2 = $suivant2Min or $suivant2 = $suivant2Maj)
						then fn:index-of($loc3,concat($noeudMin,' ',$suivantMin,' ',$suivant2Min)) else 0"/>
					<!--<xsl:message select="concat($noeud,' ',$suivantMin,' ',$suivant2Min,' ',$pos)"/>-->
					<xsl:variable name="pbSuivant3" as="xs:boolean" select="ends-with($loc3Br[$pos],substring(
						if(local-name($suivant3)='ponctuation' or $suivant2/@doSpace='false') then 
						translate($suivant3,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''','&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')  else 'A',1,1))"/>
					<xsl:choose>
						<xsl:when test="$pos > 0 and not($pbSuivant3)">
							<xsl:element name="mot" inherit-namespaces="no">
								<xsl:attribute name="abr" select="$loc3Br[$pos]"/>
								<xsl:copy-of select="$suivant2/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
								<xsl:value-of select="concat($noeud,'_',$suivant,'_',$suivant2)"/>
							</xsl:element>
							<xsl:call-template name="litPass1Abr">
								<xsl:with-param name="noeud" select="$noeud/following-sibling::*[3]"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="pos2" as="xs:integer?" select="fn:index-of($loc2,concat($noeudMin,' ',$suivantMin))"/>
							<xsl:variable name="pbSuivant2" as="xs:boolean" select="ends-with($loc2Br[$pos2],substring(
						if(local-name($suivant2)='ponctuation' or ($suivant/@doSpace='false')) then 
						translate($suivant2,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''','&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')  else 'A',1,1))"/>
<!--							<xsl:message select="'pbSuiv2', $pbSuivant2, 'suiv=',$suivant2"/> -->
							<xsl:choose>
								<xsl:when test="$pos2 > 0 and not($pbSuivant2)">
									<xsl:element name="mot" inherit-namespaces="no">
										<xsl:attribute name="abr" select="$loc2Br[$pos2]"/>
										<xsl:copy-of select="$suivant/@*"/><!-- on ne prend que les derniers attr --><!--TODO améliorer si attr différents-->
										<xsl:value-of select="concat($noeud,'_',$suivant)"/>
									</xsl:element>
									<xsl:call-template name="litPass1Abr">
										<xsl:with-param name="noeud" select="$noeud/following-sibling::*[2]"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="litPass1AbrSignes">
										<xsl:with-param name="noeud" select="$noeud"/>
									</xsl:call-template>
									<xsl:call-template name="litPass1Abr">
										<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1]"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="litPass1AbrSignes">
						<xsl:with-param name="noeud" select="$noeud"/>
					</xsl:call-template>
					<xsl:call-template name="litPass1Abr">
						<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1]"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="litPass1AbrSignes">
				<xsl:with-param name="noeud" select="$noeud"/>
			</xsl:call-template>
			<xsl:call-template name="litPass1Abr">
				<xsl:with-param name="noeud" select="$noeud/following-sibling::*[1]"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="litPass1AbrSignes"><!--valable aussi pour les locutions sur un mot et les invariants-->
	<xsl:param name="noeud" as="element()"/>
	<xsl:variable name="motMin" as="xs:string" select="fn:lower-case($noeud)"/>
	<!-- la transcription du mot est-elle possible directement? -->
	<xsl:variable name="pos" as="xs:integer?" select="fn:index-of(($signes,$loc1),$motMin)"/>
	<!--<xsl:message select="('pass1signes',$motMin,';',$pos)"/>-->
	<xsl:variable name="suivant" select="$noeud/following-sibling::*[1]" as="element()?"/>
	<xsl:variable name="pbSuivant" as="xs:boolean" select="ends-with(($signesBr,$loc1Br)[$pos],substring(
		if(local-name($suivant)='ponctuation' or ($noeud/@doSpace='false')) then 
		translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
			'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
			else 'A',1,1))"/>
<!-- 		<xsl:message select="'trans=',($signesBr,$loc1Br)[$pos],' suivant:', $suivant, ' debSuiv=',substring( -->
<!-- 		if(local-name($suivant)='ponctuation' or ($noeud/@doSpace='false')) then  -->
<!-- 		translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''', -->
<!-- 			'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;') -->
<!-- 			else 'A',1,1)"/> -->
	<xsl:variable name="pbPrec" select="$noeud/preceding-sibling::*[1]/@doSpace='false' 
		and $noeud/preceding-sibling::*[1]='-' and starts-with(($signesBr,$loc1Br)[$pos],'&pt36;')" 
		as="xs:boolean"/><!--cette dernière règle pour les tirets et les abr. en pt36;-->
<!-- 	<xsl:message select="' * Signe: pbSuiv', $pbSuivant, 'suiv=',$suivant"/> -->
	<xsl:choose>
		<xsl:when test="$pos > 0 and not($pbSuivant or $pbPrec)">
			<xsl:element name="mot" inherit-namespaces="no">
				<xsl:attribute name="abr" select="($signesBr,$loc1Br)[$pos]"/>
				<xsl:attribute name="ori" select="$noeud"/>
				<xsl:copy-of select="$noeud/@*"/>
				<xsl:value-of select="substring($noeud,1,1)"/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<!-- non; peut-on appliquer une règle de l'ensemble signe? -->
			<xsl:variable name="trans" select="nat:matchRules($motMin,$signesRules,$signesRulesBr,$signesDeriv,$signesDerivBr,1)" as="xs:string?"/>
			<xsl:choose>
				<xsl:when test="not($trans = ' ' or $pbSuivant or $pbPrec)">
					<xsl:element name="mot" inherit-namespaces="no">
						<xsl:attribute name="abr" select="$trans"/>
						<xsl:attribute name="ori" select="$noeud"/>
						<xsl:copy-of select="$noeud/@*"/>
						<xsl:value-of select="$noeud"/>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="$noeud" copy-namespaces="no"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--
	renvoie un espace si on n'a pas réussi à appliquer de règles
	ou la transcription complète du mot si on a pu appliquer une règle
-->
<xsl:function name="nat:matchRules" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<!-- mot à tester -->
	<xsl:param name="mot" as="xs:string"/> 
	<!-- liste des règles -->
	<xsl:param name="liste" as="xs:string*"/>
	<!-- liste des remplacements -->
	<xsl:param name="listeBr" as="xs:string*"/>
	<!-- liste des signes/symboles sur qui on peu appliquer la règle -->
	<xsl:param name="listePos" as="xs:string*"/>
	<!-- transcription de la liste des possibles -->
	<xsl:param name="listePosBr" as="xs:string*"/>
	<!-- position dans la liste -->
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:choose>
		<!-- c'est fini, on a rien trouvé -->
		<xsl:when test="$pos > count($liste)">
			<xsl:value-of select="' '"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="resu" as="xs:string?">
<!--				<xsl:message select="('règle:',$liste[$pos])"/>-->
				<!-- test de la règle -->
				<xsl:analyze-string select="$mot" regex="{$liste[$pos]}">
					<xsl:matching-substring>
<!--						<xsl:message select="(regex-group(1),';',regex-group(2),';',regex-group(3))"/>-->
						<xsl:choose>
							<!-- on vérifie que le groupe qui match fait bien partie de la liste des symboles/signes possibles -->
							<xsl:when test="functx:is-value-in-sequence(regex-group(1),$listePos)">
								<xsl:variable name="pos2" as="xs:integer?" select="fn:index-of($listePos,regex-group(1))"/>
								<!--<xsl:message select="($pos2,' ',$mot)"/>-->
								<xsl:value-of select="translate(fn:replace(fn:replace($mot,$liste[$pos],$listeBr[$pos]),'\(.*\)',$listePosBr[$pos2]),'s','&pt234;')"/>
							</xsl:when>
							<xsl:otherwise/>
						</xsl:choose>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:variable>
			<!--<xsl:message select="concat('resu:',$resu)"/>-->
			<xsl:value-of select="if(string-length($resu)=0) then nat:matchRules($mot,$liste,$listeBr,$listePos,$listePosBr,$pos+1) else $resu"/>
			<!--<xsl:value-of select="fn:index-of($liste,$mot)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<xsl:function name="nat:matchRulesP2" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<!-- mot à tester -->
	<xsl:param name="mot" as="xs:string"/> 
	<!-- liste des règles -->
	<xsl:param name="liste" as="xs:string*"/>
	<!-- liste des remplacements -->
	<xsl:param name="listeBr" as="xs:string*"/>
	<!-- position dans la liste -->
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:choose>
		<!-- c'est fini, on a rien trouvé -->
		<xsl:when test="$pos > count($liste)">
			<xsl:value-of select="' '"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="resu" as="xs:string?">
<!--				<xsl:message select="('règle:',$liste[$pos])"/>-->
				<!-- test de la règle -->
				<xsl:analyze-string select="$mot" regex="{$liste[$pos]}">
					<xsl:matching-substring>
<!--						<xsl:message select="(regex-group(1),';',regex-group(2),';',regex-group(3))"/>-->
						<xsl:if test="index-of(($symbsBr,$symbsNCBr),regex-group(1)) > 0">
							<xsl:value-of select="fn:replace($mot,$liste[$pos],$listeBr[$pos])"/>
						</xsl:if>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:variable>
			<!--<xsl:message select="concat('resu:',$resu)"/>-->
			<xsl:value-of select="if(string-length($resu)=0) then nat:matchRulesP2($mot,$liste,$listeBr,$pos+1) else $resu"/>
			<!--<xsl:value-of select="fn:index-of($liste,$mot)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<xsl:template name="bijection">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:call-template name="symbolesComposes">
		<xsl:with-param name="mot" select="lower-case($mot)"/>
		<xsl:with-param name="PN" select="$PN"/>
	</xsl:call-template>
</xsl:template>

<xsl:template name="symbolesComposes">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer"/>
	<xsl:param name="tiret" select='0' as="xs:integer"/><!-- = 1 si il faut tester le tiret simple; 2 si on est en fin de mot-->
<!-- 	<xsl:message select="('pass2:',.)"/> -->
	<xsl:choose>
		<xsl:when test="@abr"><!-- le mot a déjà été abrégé -->
			<xsl:value-of select="@abr"/>
		</xsl:when>
		<xsl:when test="@doSpace='false' and $mot='-'">
			<xsl:text>&pt36;</xsl:text>
<!-- <xsl:message select="' * trait d''union'"/> -->
		</xsl:when>
		<!-- mot à ne pas abréger -->
		<xsl:when test="../@abreger='false' or @abreger='false'">
			<!--<xsl:message select="'   ** pas d''abr ** pour', $mot"/>-->
			<xsl:if test="@ivb='true'"><xsl:text>&pt56;</xsl:text></xsl:if><!-- ajout de l'ivb si besoin -->
			<xsl:call-template name="finBijection">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="rezu">
				<xsl:call-template name="symboleFond">
					<xsl:with-param name="mot" select="lower-case($mot)"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:value-of select="$rezu"/>
			<!--<xsl:message select="concat('mot:', $mot, 'rezu:', $rezu)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="symboleFond">
	<xsl:param name="mot"/><!-- il est en minuscule à partir d'ici -->
	<!--<xsl:message select="($symbs,$mot)"/>-->
	<xsl:variable name="pos" as="xs:integer?" select="fn:index-of(($symbs,$symbsNC),$mot)"/>
	<xsl:choose>
		<xsl:when test="$pos > 0"><!--oui-->
			<xsl:variable name="trans" select="($symbsBr,$symbsNCBr)[$pos]" as="xs:string"/>
<!-- 			<xsl:message select="' * Symb fond:',$trans, ' ', preceding-sibling::*[1]"/> -->
			<xsl:variable name="pbPrec" as="xs:boolean" select="preceding-sibling::*[1]/@doSpace='false' and
						ends-with(translate(preceding-sibling::*[1],'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
						'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;'),
						substring($trans,1,1))"/>
			<xsl:variable name="suivant" select="following-sibling::*[1]" as="element()?"/>
			<xsl:variable name="pbSuivant" as="xs:boolean" select="ends-with(($symbsBr,$symbsNCBr)[$pos],substring(
				if(local-name($suivant)='ponctuation' or (@doSpace='false')) then 
				translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
					'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
					else 'A',1,1))"/>
<!-- 			<xsl:message select="'trans=',($symbsBr,$symbsNCBr)[$pos],' suivant:', $suivant, ' debSuiv=',substring( 
				if(local-name($suivant)='ponctuation' or (@doSpace='false')) then 
				translate($suivant,'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
				'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
				else 'A',1,1)"/>
			<xsl:message select="' * Signe: pbSuiv', $pbSuivant, 'suiv=',$suivant"/>-->
			<xsl:choose>
				<xsl:when test="$pbPrec or $pbSuivant">
					<xsl:call-template name="casGeneral">
						<xsl:with-param name="mot" select="$mot"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$trans"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<!-- est-ce que le mot contient un symbole dérivable -->
			<xsl:choose>
				<xsl:when test="functx:contains-any-of($mot,$symbs) or nat:starts-with-any-of($mot,$symbsNC)">
					<!--extraction du symbole -->
					<!--<xsl:message select="('resultat-match:',replace('destinations', '.*(destination|nation).*','patt:$1'))"/>-->
					<xsl:variable as="xs:string*" name="symbPref">
						<xsl:analyze-string select="$mot" regex="{concat('(',string-join(($symbs,$symbsNC),'|'),')')}">
							<xsl:matching-substring>
								<xsl:value-of select="regex-group(1)"/>
							</xsl:matching-substring>
						</xsl:analyze-string>
					</xsl:variable>
					<!--<xsl:variable name="symb" as="xs:string?" select="replace($mot,concat('.*(',string-join($symbs,'|'),')'),'$1')"/>-->
<!-- 					<xsl:message select="(' *** COMPOSé? prefixe:',substring-before($mot,$symbPref[1]),';mot:',$mot,'symb:',$symbPref[1])"/> -->
					<xsl:variable name="prefixe" as="xs:string?" select="substring-before($mot,$symbPref[1])"/>
					<xsl:variable name="reste" select="if(string-length($prefixe)=0) then $mot else substring-after($mot,$prefixe)" as="xs:string"/>
<!--					<xsl:message select="('oui',' m:',$mot,' p:',$prefixe,' s:',$symbPref[1],' r:',$reste,'taille:',count($symbPref))"/>-->					
					<xsl:variable name="prefBr">
						<xsl:if test="not($reste = $mot)"><!-- il y a un préfixe et le mot est composable-->
							<xsl:call-template name="casGeneral">
								<xsl:with-param name="mot" select="$prefixe"/><!-- à améliorer: pb pour tout ce qui match au début en cas gen -->
								<xsl:with-param name="termin" select="false()"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:variable>
					<xsl:value-of select="$prefBr"/>
					<!-- dérivé? -->
					<!-- non; peut-on appliquer une règle de l'ensemble symbole? -->
					<xsl:variable name="trans" select="nat:matchRules($reste,$symbsRules,$symbsRulesBr,($symbs,$symbsNC),($symbsBr,$symbsNCBr),1)" as="xs:string?"/>
					<xsl:choose>
						<!-- est-ce que le mot précédent est collé à celui-ci, et dans ce cas est-ce qu'il n'y a pas deux signes identiques qui se suivent? -->
						<xsl:when test="(preceding-sibling::*[1]/@doSpace='false' or string-length($prefBr) > 0) and
							ends-with(translate(if(string-length($prefBr) > 0) then $prefBr else preceding-sibling::*[1],
							'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''', '&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;'),
							substring($trans,1,1))"><!--if(string-length($prefBr)>0) then $prefBr else $trans,1,1-->
							<xsl:call-template name="casGeneral">
								<xsl:with-param name="mot" select="$reste"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="not($trans = ' ')">
							<xsl:value-of select="$trans"/>
						</xsl:when>
						<xsl:otherwise>
<!--							<xsl:message select="'essai pass 2'"/>-->
							<xsl:variable name="indexSymb" select="index-of(($symbs,$symbsNC),$symbPref[1])[1]" as="xs:integer?"/>
							<!-- règles de pass2 -->
							<xsl:variable name="trans2" select="nat:matchRulesP2(fn:replace($reste,$symbPref[1],($symbsBr,$symbsNCBr)[$indexSymb]),$symbsRulesP2,$symbsRulesP2Br,1)" as="xs:string?"/>
<!--							<xsl:message select="'index:', $indexSymb,'symb',$symbPref[1],'br',($symbsBr,$symbsNCBr)[$indexSymb],'trans:',$trans2"/>-->
							<xsl:choose>
								<xsl:when test="not($trans2 = ' ')">
									<xsl:value-of select="$trans2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="casGeneral">
										<xsl:with-param name="mot" select="$reste"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="casGeneral">
						<xsl:with-param name="mot" select="$mot"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="casGeneral">
	<xsl:param name="mot"/>
	<xsl:param name="termin" select="true()" as="xs:boolean"/>
	<xsl:choose>
		<!-- le mot ne doit pas être abrégé -->
		<xsl:when test="matches($mot,$l_ivb) and $termin and not(preceding-sibling::*[1]/@doSpace='false' or @doSpace='false')">
			<!--<xsl:element name="mot" inherit-namespaces="no">
				<xsl:attribute name="abreger" select="'false'"/>
				<xsl:attribute name="ivb" select="'true'"/>
				<xsl:copy-of copy-namespaces="no" select="$noeud"/>
			</xsl:element>-->
			<xsl:text>&pt56;</xsl:text>
			<xsl:call-template name="finBijection">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<!-- caractère de fin du mot précédent s'il est collé -->
			<xsl:variable name="finPrec" as="xs:string" select="if(preceding-sibling::*[1]/@doSpace='false')
					then translate(substring(preceding-sibling::*[1],string-length(preceding-sibling::*[1]),1),
							'-,;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿''',
							'&pt36;&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;&pt3;')
					else ('&pt;')"/>

			<xsl:variable name="rules" as="xs:string*" select="if ($termin) then ($cgenFinRules,$cgenRules,$cgenPass2Rules) else $cgenRules"/>
			<xsl:variable name="rulesBr" as="xs:string*" select="if ($termin) then ($cgenFinRulesBr,$cgenRulesBr,$cgenPass2RulesBr) else $cgenRulesBr"/>

			<xsl:variable name="motAbr" as="element()*" select="nat:matchGeneralRules($mot,$rules,$rulesBr,1)"/>

<!-- 			<xsl:message select="('mot abr:', $motAbr, ':' ,  string-join($motAbr/noir,''), ':', string-join($motAbr/abr,''))"/> -->
			<!-- tri des séquences -->
			<xsl:variable name="motAbrSort" as="element()*">
				<xsl:for-each select="$motAbr">
					<xsl:sort select="./deb" data-type="number"/>
					<xsl:sort select="string-length(./noir)" order="descending" data-type="text"/>
					<xsl:copy-of select="."/>
				</xsl:for-each>
			</xsl:variable>
<!-- 			<xsl:message select="('abr sort:', $motAbrSort, ':' ,  string-join($motAbrSort/noir,''), ':', string-join($motAbrSort/abr,''))"/> -->
			<xsl:variable name="lg" as="xs:double*" select="(for $i in $motAbrSort return $i/deb + string-length($i/noir))"/>
<!-- 			<xsl:message select="('lg:',$lg)"/> -->
			<xsl:variable name="pbPrec" as="xs:boolean" select="if($motAbrSort[1]/deb=0 and $motAbrSort[1]/abr = $finPrec) then true() else false()"/>
			<xsl:choose>
				<!-- il n'y a pas d'empiètement partiel dans les abréviations -->
				<xsl:when test="every $i in 1 to (count($lg) - 1) satisfies 
					(every $j in 1 to (count($lg) - 1) satisfies ($lg[$i] &lt;= number($motAbrSort[$j]/deb)) or 
					($lg[$i] >= $lg[$i+1] and number($motAbrSort[$i]/deb) &lt;= number($motAbrSort[$i+1]/deb)))
					and not($lg[$i] = number($motAbrSort[$i+1]/deb) and $motAbrSort[$i]/abr = $motAbrSort[$i+1]/abr and not($motAbrSort[$i]/noir = $motAbrSort[$i+1]/noir)) ">
<!-- 					<xsl:message select="'- - pas d''empiètement - -'"/> -->
					<!-- il peut y avoir une inclusion avec le >= (ex: ellement et ent), mais dans ce cas le replace du ent final sera vide et non effectué -->
					<!--écriture des abréviations -->			
					<xsl:variable name="chSyll" as="xs:string*" select="if($pbPrec) then ($motAbrSort[position()>1]/noir) else (($motAbrSort/noir))"/><!-- syllabes noires abrégées -->
					<xsl:variable name="chSyllAbr" as="xs:string*" select="if($pbPrec) then ($motAbrSort[position()>1]/abr) else (($motAbrSort/abr))"/><!-- syllabes abrégées -->
					
					<xsl:variable name="finAbr">
						<xsl:variable name="apresAbr" select="nat:replace-first-multi($mot,$chSyll,$chSyllAbr)"/>
						<xsl:value-of select="if (functx:contains-any-of($apresAbr, $l_pt_sup) or string-length($apresAbr) &lt; 2) then $apresAbr 
							else (nat:replace-first-multi($mot,$chSyll[position()>1],$chSyllAbr[position()>1]))"/>
					</xsl:variable>
					<xsl:call-template name="finBijection">
						<xsl:with-param name="mot" select="$finAbr"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<!-- il y a empiètement partiel (ex: ien + ent, prompt -> pro + pr + om => pr + om et pas pro + m); on doit décider quelle abréviation conserver -->
<!-- 					<xsl:message select="'- - empiètement - -'"/> -->
					<xsl:variable as="element()*" name="motAbrSort2">
						<!--<saxon:iterate select="1 to count($motAbrSort)" xmlns:saxon="http://saxon.sf.net/" xsl:extension-element-prefixes="saxon">-->
						<xsl:for-each select="1 to count($motAbrSort)">
							<!--<xsl:param name="erase" as="xs:boolean" select="false()"/> 
									On peut se passer du erase: ma séquence n'arrivera pas à se déclencher car elle sera détruite par la précédente
									Vérifier quand même dans le cas d'une utilisation de deux séquences genre dissitrucasse, ss
								-->
							<xsl:variable name="i" select="." as="xs:integer"/>
							<!-- liste des syllabes en conflit de recouvrement -->
							<xsl:variable name="conflit" as="element()*" 
								select="for $j in $i + 1 to count($lg) return
								if ($lg[$i] &gt; number($motAbrSort[$j]/deb)) then $motAbrSort[$j] else()"/>
<!-- 							<xsl:message select="(' - en conflit avec ', $motAbrSort[$i], ' :', $conflit)"/> -->
							<xsl:variable name="syllabe" as="element()?">
								<xsl:choose>
									<xsl:when test="position()=last() or count($conflit)=0">
										<xsl:choose>
											<!-- si l'abréviation suivante est la même mais que le noir est différent => on ne garde pas l'assemblage -->
											<xsl:when test="$lg[$i] = number($motAbrSort[$i + 1]/deb) and $motAbrSort[$i]/abr = $motAbrSort[$i + 1]/abr and not($motAbrSort[$i]/noir = $motAbrSort[$i + 1]/noir)">
<!-- 												<xsl:message select=" ('  * Séquence d''assemblages non compatible:', $motAbrSort[$i]/noir, ' et ' , $motAbrSort[$i + 1]/noir)"/> -->
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="$motAbrSort[$i]"/><!--<xsl:message select="'dernier'"/>-->
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable as="xs:integer*" name="valide">
											<xsl:for-each select="$conflit">
												<xsl:variable as="xs:integer" select="position()" name="j"/>
												<!--<xsl:message select="('lg $i:', $lg[$i], ' $conflit[$j]/deb:', number($conflit[$j]/deb),
													' $motAbrSort[$i]/abr:', $motAbrSort[$i]/abr, ' $conflit[$j]/abr:', $conflit[$j]/abr, ' noirs:', $motAbrSort[$i]/noir, ' ',$conflit[$j]/noir)"/>-->
												<xsl:choose>
													<!-- ne pas utiliser l'abréviation -->
													<!--<xsl:when test="$erase" />-->
													<!-- si l'abréviation suivante est la même mais que le noir est différent => on ne garde pas l'assemblage -->
													<xsl:when test="$lg[$i] = number($conflit[$j]/deb) and $motAbrSort[$i]/abr = $conflit[$j]/abr and not($motAbrSort[$i]/noir = $conflit[$j]/noir)">
<!-- 														<xsl:message select=" ('  * Séquence d''assemblages non compatible:', $motAbrSort[$i]/noir, ' et ' , $conflit[$j]/noir)"/> -->
														<xsl:value-of select="0"/>
													</xsl:when>
													<!-- pas de pb avec la syllable actuelle -->
													<xsl:when test="$lg[$i] &lt;= number($conflit[$j]/deb) or ($lg[$i] >= $lg[$i+$j] 
														and number($motAbrSort[$i]/deb) &lt;= number($conflit[$j]/deb))">
														<xsl:value-of select="1000"/>
													</xsl:when>
													<!-- il y a un recouvrement, mais l'abréviation en conflit fait partie de la liste des pass2 -->
													<xsl:when test="functx:is-value-in-sequence($conflit[$j]/abr, $cgenPass2RulesBr)">
														<xsl:value-of select="1000"/>
													</xsl:when>
													<!-- la sequence fait partie des pass2 et il y a recouvrement -->
													<xsl:when test="$i > 1 and functx:is-value-in-sequence($motAbrSort[$i]/abr, $cgenPass2RulesBr) and
															$lg[$i - 1] > $lg[$i]">
														<xsl:value-of select="0"/>
													</xsl:when>
													<!-- la séquence n'appartient pas à une ambiguité connue-->
													<xsl:when test="every $ch in $l_amb_connues satisfies not(contains($ch,$motAbrSort[$i]/noir)) ">
														<xsl:for-each select="$l_amb_connues">
<!-- 															<xsl:message select="(not(contains($motAbrSort[$i]/noir,.)), 'val:', ., 'Abr:', $motAbrSort[$i]/noir)"/> -->
														</xsl:for-each>
<!-- 														<xsl:message select="' EMPIET:',$motAbrSort[$i],' SUIV:',$motAbrSort[$i+$j], ' conflit:', $conflit[$j]"/> -->
														<xsl:variable name="motDecoup" as="xs:string" select="nat:toBlack(nat:hyphenate(nat:toBrUTF8($mot),$coupe))"/>
<!-- 														<xsl:message select="('  - mot decoup:', $motDecoup)"/> -->
														<!-- première tentative: on conserve la première abréviation uniquement si elle recouvre une syllable -->
														<xsl:variable name="decoupChars" select="functx:chars($motDecoup)" as="xs:string*"/>
<!-- 														<xsl:message select="('  - decoupCh:', $decoupChars)"/> -->
<!-- 														<xsl:message select="'sous-chaine', substring($motDecoup,1,string-length(replace(substring($motDecoup,1,$lg[$i]),concat('[^',$coupe,']'),'')) + $lg[$i] +1),translate(substring($motDecoup,1,$lg[$i]),$coupe,'')"/> -->
														<!-- c'est là qu'il faut prioriser les abréviations -->
														<xsl:value-of select="if (ends-with(substring($motDecoup,1,string-length(replace(substring($motDecoup,1,$lg[$i]),concat('[^',$coupe,']'),'')) + $lg[$i] +1),$coupe)) then 1000 else(0)"/>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="2"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</xsl:variable>
										<xsl:if test="not(functx:is-value-in-sequence(0,$valide))">
											<xsl:choose>
												<xsl:when test="functx:is-value-in-sequence(2,$valide)">
													<xsl:element name="amb">
														<xsl:element name="truc">et oui</xsl:element>
													</xsl:element>
<!-- 													<xsl:message select="'** génération de l''ambiguité'"/> -->
												</xsl:when>
												<xsl:otherwise><xsl:copy-of select="$motAbrSort[$i]"/></xsl:otherwise>
											</xsl:choose>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:copy-of select="$syllabe"/>
		<!--					<xsl:message select="'   conservé:',$syllabe"/>-->
							<!--<saxon:continue/>
								<xsl:with-param name="erase" select="if($syllabe/@eraseNext='yes') then true() else (false())"/>
							</saxon:continue>-->
						</xsl:for-each>
						<!--</saxon:iterate>-->
					</xsl:variable>
					
<!-- 					<xsl:message select="(count($motAbrSort2),$motAbrSort2,count($motAbrSort2/amb),$motAbrSort2/noir)"/> -->
					<xsl:choose>
						<xsl:when test="count(($motAbrSort2/amb))=0">
							<!--écriture des abréviations -->			
							<xsl:variable name="chSyll" as="xs:string*" select="($motAbrSort2/noir)"/><!-- syllabes noires abrégées -->
							<xsl:variable name="chSyllAbr" as="xs:string*" select="($motAbrSort2/abr)"/><!-- syllabes abrégées -->

							<xsl:variable name="finAbr">
								<xsl:variable name="apresAbr" select="nat:replace-first-multi($mot,$chSyll,$chSyllAbr)"/>
								<xsl:value-of select="if (functx:contains-any-of($apresAbr, $l_pt_sup) or string-length($apresAbr) &lt; 2) then $apresAbr 
									else (nat:replace-first-multi($mot,$chSyll[position()>1],$chSyllAbr[position()>1]))"/>
							</xsl:variable>
				<!--			<xsl:message select="('séquence recouvertes:',$chSyll, ';', $chSyllAbr)"/>-->
							<xsl:call-template name="finBijection">
								<xsl:with-param name="mot" select="$finAbr"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
<!-- 							<xsl:message select="('ambi:',$motAbrSort2/amb)"/> -->
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- based on functx:replace-multi -->
<xsl:function name="nat:replace-first-multi" as="xs:string?" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="arg" as="xs:string?"/> 
	<xsl:param name="changeFrom" as="xs:string*"/> 
	<xsl:param name="changeTo" as="xs:string*"/> 

	<xsl:sequence select=" 
		if (count($changeFrom) > 0)
		then nat:replace-first-multi(
					functx:replace-first($arg, $changeFrom[1],
											functx:if-absent($changeTo[1],'')),
					$changeFrom[position() > 1],
					$changeTo[position() > 1])
		else $arg
 "/>
   
</xsl:function>

<!--
	renvoie une séquence contenant les syllabes abrégées
-->
<xsl:function name="nat:matchGeneralRules" as="element()*" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<!-- mot à tester -->
	<xsl:param name="mot" as="xs:string"/> 
	<!-- liste des règles -->
	<xsl:param name="liste" as="xs:string*"/>
	<!-- liste des remplacements -->
	<xsl:param name="listeBr" as="xs:string*"/>
	<!-- position dans la liste -->
	<xsl:param name="pos" as="xs:integer"/>
	<xsl:choose>
		<!-- c'est fini, on a rien trouvé -->
		<xsl:when test="$pos > count($liste)"/>
			<!--<xsl:element name="syll" inherit-namespaces="no">
				<xsl:element name="noir" inherit-namespaces="no"><xsl:value-of select="$mot"/></xsl:element>
				<xsl:element name="abr" inherit-namespaces="no"><xsl:value-of select="$mot"/></xsl:element>
			</xsl:element>
		</xsl:when>-->
		<xsl:otherwise>
			<xsl:variable name="resu" as="xs:string*">
				<!-- test de la règle -->
				<xsl:analyze-string select="$mot" regex="{$liste[$pos]}">
					<xsl:matching-substring>
						<!--<xsl:message select="('match-subst:',.)"/>
						<xsl:message select="('match:',regex-group(1),';',regex-group(2),';',regex-group(3))"/>-->
						<xsl:sequence select="(regex-group(1),$listeBr[$pos],regex-group(3),regex-group(2))"/>
					</xsl:matching-substring>
				</xsl:analyze-string>
			</xsl:variable>
			<!--<xsl:message select="concat('resu:',$resu)"/>-->
			<!--<xsl:message select="($resu, ' 1:',$resu[1],' 2:',$resu[2],' 3:',$resu[3])"/>-->
			<!--<xsl:message select="(' - nb matches pour la règle: ', count($resu))"/>-->
			<xsl:for-each select="1 to count($resu) div 4">
				<xsl:variable name="i" select="4*(. - 1)" as="xs:integer"/>
				<!--<xsl:message select="(count($resu),':',$i,':',$resu[$i+2])"/>-->
				<xsl:if test="string-length($resu[$i+2])>0">
					<xsl:element name="syll" inherit-namespaces="no">
						<xsl:element name="deb" inherit-namespaces="no">
							<!--<xsl:message select="('positionDeb:', string-join(tokenize($mot, $resu[$i+4]),''),'mot:', string-join(tokenize($mot, $resu[$i+4])[position() &lt;= $i div 4  + 1],''))"/>-->
							<xsl:value-of select="if (string-length($resu[$i+1]) > 0) then
								string-length($resu[$i+1]) else ($i div 4 * string-length($resu[$i+4]) + string-length(string-join(tokenize($mot, $resu[$i+4])[position() &lt;= $i div 4  + 1],'')))"/>
						</xsl:element>
						<xsl:element name="noir" inherit-namespaces="no"><xsl:value-of select="$resu[$i+4]"/></xsl:element>
						<xsl:element name="abr" inherit-namespaces="no"><xsl:value-of select="$resu[$i+2]"/></xsl:element>
					</xsl:element>
				</xsl:if>
			</xsl:for-each>
			<xsl:sequence select="nat:matchGeneralRules($mot,$liste,$listeBr,$pos+1)"/>
			<!--<xsl:sequence select="if(string-length($resu[2])=0) 
					then nat:matchGeneralRules($mot,$liste,$listeBr,$pos+1) else ($resu)"/>-->
			<!--<xsl:value-of select="fn:index-of($liste,$mot)"/>-->
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>

<!-- return true if the contracted word doesn't begin with the preceding word, if there is no space between them
param: word: the word to test, in braille
param: prec: the preceding word without space
 -->
<xsl:function name="nat:isContractableDeb" as="xs:boolean" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
	<xsl:param name="word" as="xs:string"/>
	<xsl:param name="prec" as="xs:string"/>
	<xsl:choose>
		<xsl:when test="string-length($prec)=0"><xsl:value-of select="true()"/></xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="not(starts-with($word, substring($prec,string-length($prec),1)))"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:function>
</xsl:stylesheet>