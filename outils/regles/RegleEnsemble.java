/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package outils.regles;

import java.util.ArrayList;

/**
 * Regroupe les règles qui s'appliquent sur des ensembles (Locutions, signes, cas général, etc.)
 * @author bruno
 *
 */
public class RegleEnsemble extends Regle implements Comparable<RegleEnsemble>
{
	/**
	 * Liste des ensembles sur lesquelles s'appliquent la règle
	 */
	protected ArrayList<String> application = new ArrayList<String>();
	
	/** identifiant de la règle */
	protected int id;
	/** reg exp à appliquer */
	protected String regIn;
	/** reg exp de remplacement */
	protected String regOut;
	/** vrai si règle à appliquer en deuxième passe */
	protected boolean pass2=false;
	
	/**
	 * Constructeur
	 * @param d description de la règle
	 * @param ref référence de la règle
	 * @param in regexp à appliquer
	 * @param o  regexp de remplacement
	 * @param a liste des ensembles sur lesquels elle s'applique
	 * @param p2 vrai si règle de deuxième passe
	 * @param i identifiant absolu et unique de la règle
	 */
	public RegleEnsemble(String d, String ref, String in, String o, ArrayList<String> a, boolean p2, int i)
	{
		super(d, ref);
		application = a;
		id = i;
		regIn = in;
		regOut = o;
		pass2 = p2;
	}
	
	/**
	 * Renvoie {@link #regIn}
	 * @return {@link #regIn}
	 */
	public String getRegIn(){return regIn;}
	
	/**
	 * Renvoie {@link #regOut}
	 * @return {@link #regOut}
	 */
	public String getRegOut(){return regOut;}
	/**
	 * Renvoie le nom et la descritpion de la règle,
	 * ainsi que la liste des ensembles sur lesquels elle s'applique
	 * @see outils.regles.Regle#toString()
	 */
	@Override
	public String toString()
	{
		String appli = "";
		for(String s:application){appli += s + " ";}
		return description + " ("+reference+"): s'applique sur " + appli;
	}

	/**
	 * Renvoie vrai si <code>ensemble</code> fait partie de la liste des ensembles {@link #application}
	 * @param ensemble le nom de l'ensemble
	 * @return vrai si {@link #application} contient <code>ensemble</code>
	 */
	public boolean isFor(String ensemble)
	{
		boolean retour = false;
		if(application.contains(ensemble)){retour=true;}
		return retour;
	}

	/**
	 * Implémentation ed Comparable
	 * Les règles sont classées suivant leur identifiant ({@link #id}
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RegleEnsemble re)
	{
		return id - re.id;
	}

	/** 
	 * Vrai si les identifiants sont identiques
	 */
	@Override
	public boolean equals(Object r)
	{
		return (r instanceof RegleEnsemble) && id == ((RegleEnsemble) r).id;
	}

	/**
	 * @see outils.regles.Regle#getXML()
	 */
	@Override
	public String getXML()
	{
		String ap ="";
		for(String s : application){ap+=("\t\t<for>"+s+"</for>\n");}
		return "\t<rule id=\""+id+"\">\n" +
				"\t\t<desc>"+description+"</desc>\n" +
				"\t\t<ref>"+reference+"</ref>\n" + ap +
				"\t\t<regIn>" + regIn + "</regIn>\n" +
				"\t\t<regOut>" + regOut + "</regOut>\n" +
				"\t</rule>\n";
	}
	/**
	 * Renvoie {@link #id}
	 * @return l'identifiant de la règle
	 */
	public int getId(){return id;}

	/**
	 * indique si la règle est à appliquer en deuxième passe
	 * @return {@link #pass2}
	 */
	public boolean isPass2()
	{
		return pass2;
	}
}
