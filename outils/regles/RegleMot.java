package outils.regles;


/**
 * Classe de regrouppement des règles portant sur l'abréviation de mot (Locution, symboles, signes)
 * Ces règles génèrent les listes de mots et leurs abréviations
 * @author bruno
 *
 */
public abstract class RegleMot extends Regle implements Comparable<RegleMot>
{
	/** Mot(s) en noir */
	protected String noir;
	/** transcription */
	protected String braille;
	
	/**
	 * @param d description
	 * @param r référence
	 * @param n mot(s) en noir
	 * @param b transcription en braille
	 */
	public RegleMot(String d, String r, String n, String b)
	{
		super(d,r);
		noir = n;
		braille = b;
	}
	
	/**
	 * Renvoie le(s) mot(s) en noir
	 * @return {@link #noir}
	 */
	public String getNoir(){return noir;}
	/**
	 * Renvoie la transcription de {@link #noir} en braille
	 * @return {@link #braille}
	 */
	public String getBraille(){return braille;}
	
	/**
	 * Tri des mots par longueur
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RegleMot rm) {
		return rm.getNoir().length()-noir.length();
	}
}
