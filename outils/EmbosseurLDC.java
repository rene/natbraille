/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package outils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import gestionnaires.GestionnaireErreur;
import nat.ConfigNat;
import nat.Nat;
import outils.TextConverter;
/**
 * Gère l'embossage par exécution d'une ligne de commande
 * @author bruno
 *
 */
public class EmbosseurLDC extends Embosseur
{
	/** la ligne de commande à exécuter pour l'embossage */
	private String ldc = "";
	/**
	 * Constructeur
	 * @param f L'adresse du fichier à embosser
	 * @param g une instance de {@link GestionnaireErreur}
	 */
	public EmbosseurLDC(String f, GestionnaireErreur g){super(f, g);}
	/**
	 * Méthode d'accès en écriture à {@link #ldc}
	 * <p>Remplace dans le paramètre s les <code>$f</code> par le paramètre <code>fic</code> représentant
	 * l'adresse du fichier à embosser
	 * @param s la ligne de commande telle que reçue des paramètres de configuration
	 * @param fic la vraie adresse du fichier à embosser
	 */
	public void setLdc(String s, String fic)
	{	
		gest.afficheMessage("Ligne de commande:" + s, Nat.LOG_DEBUG);
		ldc = s.replace("$f", fic);
		gest.afficheMessage("Ligne de commande avec fichier:" + ldc, Nat.LOG_DEBUG);
	}
	/**
	 * Fabrique un script d'exécution conteannt les commandes d'embossage
	 * <p>Suivant le système d'exploitation (Windows ou Unix), crée des scripts différents</p>
	 */
	public void fabriqueExec()
	{
		File fff = new File(fichier);
		gest.afficheMessage("\n***Système d'exploitation: "+ SystemUtils.OS_NAME + ",ver: "+SystemUtils.OS_VERSION,Nat.LOG_VERBEUX);
		gest.afficheMessage("\n***Création du script", Nat.LOG_VERBEUX);
		if(SystemUtils.IS_OS_UNIX)
		{
			setLdc(ConfigNat.getCurrentConfig().getCommande(), fff.getPath()); //ldc est assignée
			try
			{
				BufferedWriter fich = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ConfigNat.getUserTempFolder()+"emboss.bat"),"UTF-8"));
				//FileWriter fichierXSL = new FileWriter(filtre);
				fich.write("#!/bin/sh\n#Generated file/fichier genere par NAT\n");
				fich.write("exec=exec\n");
				fich.write("exec " + ldc + ";");
				fich.close();
				new File(ConfigNat.getUserTempFolder()+"emboss.bat").setExecutable(true);
			}
			catch (IOException e)
			{
				gest.afficheMessage("\nErreur lors de la création du script d'embossage: " +e.getMessage() ,Nat.LOG_SILENCIEUX);
				
			}
		}
		else if(SystemUtils.IS_OS_WINDOWS)
		{
			/* TextConverter f = new TextConverter (fichier);
			try {
					f.convert(); //conversion des lf en crlf pour impression
				} catch(Exception e) {System.err.println("Erreur");} */
			setLdc(ConfigNat.getCurrentConfig().getCommande(), fff.getPath()); //ldc est assignée			
			try
			{
				BufferedWriter fich = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(ConfigNat.getUserTempFolder()+"emboss.bat"),"UTF-8"));
				//FileWriter fichierXSL = new FileWriter(filtre);
				fich.write(ldc);
				fich.close();
			}
			catch (IOException e)
			{
				gest.afficheMessage("\nErreur lors de la création du script d'embossage" + e,Nat.LOG_SILENCIEUX);
			}
		}
		else
		{
			gest.afficheMessage("\nPas d'implémentation actuellement pour l'os " + SystemUtils.OS_NAME,Nat.LOG_SILENCIEUX);
		}
	}
// TODO arranger le emboss.bat -> dans tmp, + emboss.sh
	/**
	 * Lance l'embossage en appelant le script réalisé dans {@link #fabriqueExec()}
	 */
	@Override
	public void Embosser() 
	{
		gest.afficheMessage("\nEmbossage", Nat.LOG_NORMAL);
		// setLdc(ConfigNat.getCurrentConfig().getCommande()); maintenant dans FabriqueExec
		if(!ConfigNat.getCurrentConfig().getCommande().equals(""))
		{
			gest.afficheMessage("système:" + SystemUtils.OS_NAME + ",ver: "+SystemUtils.OS_VERSION,Nat.LOG_DEBUG);
			fabriqueExec();
			if(SystemUtils.IS_OS_UNIX)
			{
				gest.afficheMessage("\n**Ecriture du script", Nat.LOG_VERBEUX);
				gest.afficheMessage("\n**Lancement de l'embossage", Nat.LOG_VERBEUX);
				Runtime runTime = Runtime.getRuntime();
				int res = 0;
				try
				{
					Process p = runTime.exec(ConfigNat.getUserTempFolder()+"emboss.bat");
					res = p.waitFor();
				}
				catch (IOException e) {gest.afficheMessage("\nErreur d'entrée/sortie", Nat.LOG_NORMAL);}
				catch (InterruptedException e) {gest.afficheMessage("\nErreur de communication avec l'embosseuse", Nat.LOG_NORMAL);}
				if (res != 0)
				{//le processus p ne s'est pas terminé normalement
					gest.afficheMessage("\nLe script d'embossage a renvoyé une erreur", Nat.LOG_NORMAL);
				}
				/*CommandRunner c = new CommandRunner("outils/emboss " + fichier);
				c.waitForCompletion();*/
			}
			else if(SystemUtils.IS_OS_WINDOWS)
			{
				Runtime runTime = Runtime.getRuntime();
				int res = 0;
				gest.afficheMessage("\n**Ecriture du script", Nat.LOG_VERBEUX);
				gest.afficheMessage("\n**Lancement de l'embossage", Nat.LOG_VERBEUX);
				TextConverter f = new TextConverter (fichier);
				try 
				{
					f.convert(); //conversion des lf en crlf pour impression
				}
				catch (Exception e) {gest.afficheMessage("\nErreur de TextConverter", Nat.LOG_NORMAL);}
				try
				{
					Process p = runTime.exec(ConfigNat.getUserTempFolder()+"emboss.bat");
					res = p.waitFor();
				}
				catch (IOException e) {gest.afficheMessage("\nErreur d'entrée/sortie", Nat.LOG_NORMAL);}
				catch (InterruptedException e) {gest.afficheMessage("\nErreur de communication avec l'embosseuse", Nat.LOG_NORMAL);}
				if (res != 0)
				{//le processus p ne s'est pas terminé normalement
					gest.afficheMessage("\nLe processus d'embossage a renvoyé une erreur", Nat.LOG_NORMAL);
				}
			}
			else{gest.afficheMessage("\nSystème d'exploitation inconnu", Nat.LOG_NORMAL);}

		}
		else
		{
			gest.afficheMessage("\nErreur: Pas de ligne de commande entrée", Nat.LOG_NORMAL);
		}
		gest.afficheMessage("\n--Embossage terminé", Nat.LOG_NORMAL);
	}
	/**
	 * Rédéfinition de {@link Embosseur#Embosser(String)}
	 * <p>Met à jour l'adresse du fichier à embosser</p>
	 * <p>Appel de {@link #Embosser()}</p>
	 * @param f l'adresse du fichier à embosser
	 */
	@Override
	public void Embosser(String f)
	{
		fichier = f;
		Embosser();
	}
}
