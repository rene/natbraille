<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE helpset PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 2.0//EN" "http://java.sun.com/products/javahelp/helpset_2_0.dtd">
<helpset version="2.0">
  <title>Aide de NAT</title>
  <maps>
     <homeID>top</homeID>
     <mapref location="nat.jhm"/>
  </maps>
  <view>
   <name>TableMatiere</name>
   <label>Table des mati�res</label>
   <type>javax.help.TOCView</type>
   <data>TableMatieres.xml</data>
  </view>
  <!-- On s'en fout, non?
	<view>
   <name>Index</name>
   <label>Index</label>
   <type>javax.help.IndexView</type>
  <data>index.xml</data>
  </view>
  -->
 <view>
   <name>Recherche</name>
   <label>Rechercher</label>
   <type>javax.help.SearchView</type>
  <data engine="com.sun.java.help.search.DefaultSearchEngine">JavaHelpSearch</data>
 </view>
 <presentation default="true" displayviewimages="false">
   <name>presentation</name>
   <size width="700" height="400" />
   <location x="200" y="200" />
   <title>Aide de NAT</title>
   <toolbar>
        <helpaction> javax.help.BackAction</helpaction>
        <helpaction> javax.help.ForwardAction</helpaction>
        <helpaction> javax.help.SeparatorAction</helpaction>
        <helpaction> javax.help.HomeAction</helpaction>
        <helpaction> javax.help.ReloadAction</helpaction>
        <helpaction> javax.help.SeparatorAction</helpaction>
        <helpaction> javax.help.PrintAction</helpaction>
        <helpaction> javax.help.PrintSetupAction</helpaction>
   </toolbar>
 </presentation>
 <presentation>
   <name>principal</name>
   <size width="400" height="400" />
   <location x="200" y="200" />
   <title>Aide de NAT</title>
 </presentation>
</helpset>