/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import nat.ConfigNat;
import nat.Nat;
import nat.Transcription;
//***  java.io ***
import java.io.File;
//import java.io.FileReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
// *** gestionnaires ***
import gestionnaires.GestionnaireErreur;
/**
 * Convertit un fichier XML (XHTML) au format interne
 * @author bruno
 *
 */
public class ConvertisseurXML extends Convertisseur
{
	/**
	 * Constructeur
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 */
	public ConvertisseurXML(String src, String tgt){super(src, tgt);}
	/**
	 * Convertit le fichier XML/XHTML {@link Convertisseur#source} au format interne de nat
	 * en appelant {@link #XHTML2FormatInterne(GestionnaireErreur, String)}
	 * @see nat.convertisseur.Convertisseur#convertir(gestionnaires.GestionnaireErreur)
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		tempsExecution = System.currentTimeMillis();
		boolean retour = false;
		gest.afficheMessage("ok\n** Conversion au format interne...",Nat.LOG_VERBEUX);
		retour = XHTML2FormatInterne(gest, "xsl/xhtml2interne.xsl"); 
		tempsExecution = System.currentTimeMillis() - tempsExecution;
		//gest.AfficheMessage("ok\n----Conversion terminée en " + tempsExecution + " msec.\n",Nat.LOG_SILENCIEUX);
		return retour;
	}
	/**
	 * Méthode réalisant la conversion via appel à la feuille xsl <code>xsl/xhtml2interne.xsl</code>
	 * @param gestErreur une instance de {@link GestionnaireErreur}
	 * @param filtre adresse de la feuille xsl à utiliser
	 * @return true si la conversion s'est bien passé, false sinon
	 */
	private boolean XHTML2FormatInterne(GestionnaireErreur gestErreur, String filtre)
	{
		gestErreur.afficheMessage("ok\n*** Création de la fabrique (DocumentBuilderFactory) ...",Nat.LOG_VERBEUX);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//configuration de la fabrique
		factory.setNamespaceAware(true);/*
		try{
			factory.setAttribute("indent-number", new Integer(6));
		}
		catch(IllegalArgumentException iae){}*/
		factory.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);//je mets a false: pas besoin de réseau: non, voir xhtmlDocument
		//factory.setIgnoringElementContentWhitespace(true);
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(false);
		try 
		{
			// sinon, génère parfois des null pointer exp au parsage (problème avec les simples quote)
			factory.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			gestErreur.afficheMessage("ok\n*** Parsage du document d'entrée XHTML avec SAX ...",Nat.LOG_VERBEUX);
			builder.setErrorHandler(gestErreur);
			Document doc = builder.parse(new File(source));
			doc.setStrictErrorChecking(true);
			gestErreur.afficheMessage("ok\n*** Initialisation et lecture de la feuille de style de conversion...",Nat.LOG_VERBEUX);
			TransformerFactory transformFactory = TransformerFactory.newInstance();
			StreamSource styleSource = new StreamSource(new File(filtre));
			// lire le style
			
			Transformer transform = transformFactory.newTransformer(styleSource);
			transform.setParameter("dtd",ConfigNat.getCurrentConfig().getDTD());
			transform.setParameter("processImage", ConfigNat.getCurrentConfig().getTranscrireImages());
			// conformer le transformeur au style
			DOMSource in = new DOMSource(doc);
			gestErreur.afficheMessage("ok\n*** Création du fichier au format interne ...",Nat.LOG_VERBEUX);
			// Création du fichier de sortie
			//File file = new File("tmpEntites.xhtml");
			File file = new File(Transcription.fTempXML);
			///Result resultat = new StreamResult(fichier);
			StreamResult out = new StreamResult(file);
			gestErreur.afficheMessage("ok\n*** Transformation du document interne...",Nat.LOG_VERBEUX);
			transform.transform(in, out);
			return true;
		}
		catch (Exception e)  
		{
			gestErreur.setException(e);
			gestErreur.gestionErreur();
			return false;
		}
	}
}