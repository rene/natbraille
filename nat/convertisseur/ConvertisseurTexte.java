/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

//Package maisons
import nat.Nat;
import gestionnaires.GestionnaireErreur;

//***  java.io ***
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
/**
 * Convertisseur de format texte/plain en format interne
 * @author bruno
 *
 */
public class ConvertisseurTexte extends Convertisseur
{
	//Attributs
	/** encodage de la source (par défaut, UTF-8)*/
	protected String sourceEncoding;
	
	/** caractère délimiteur de mots */
	protected char espace =' ';
	
	//"%", "‰", ponctuation? pourquoi???
	// FEINTE: on remplace ... par … dans ligne lit
	/** Tableau des ponctuations possibles en fin de mot */
	protected String[] ponctuationFin = {"-","”","’",",", ".", ":", ";", "!", "?", "»","…", ")", "]", "}","\"","*"};
	/** TAbleau des ponctuations possibles en début de mot */
	protected String[] ponctuationDebut = {"-","¡","¿","«","“","‘","(", "[", "{","\"","*"};
	/**
	 * Constructeur
	 * <p>L'encodage utilisé par défaut est UTF-8 </p>
	 * @param src l'adresse du fichier source à convertir
	 * @param tgt l'adresse du fichier cible au format interne
	 */
	public ConvertisseurTexte(String src, String tgt)
	{
		super(src, tgt);
		sourceEncoding = "UTF-8";
	}
	/**
	 * Constructeur
	 * <p>L'encodage utilisé par défaut est UTF-8 </p>
	 * @param src l'adresse du fichier source à convertir
	 * @param tgt l'adresse du fichier cible au format interne
	 * @param sep charactère séparateur de mot
	 */
	public ConvertisseurTexte(String src, String tgt, char sep)
	{
		super(src, tgt);
		sourceEncoding = "UTF-8";
		espace = sep;
	}
	/**
	 * Constructeur
	 * @param src l'adresse du fichier source à convertir
	 * @param tgt l'adresse du fichier cible au format interne
	 * @param sEncoding encodage du fichier source
	 */
	public ConvertisseurTexte(String src, String tgt,String sEncoding)
	{
		super(src, tgt);
		sourceEncoding = sEncoding;
	}
	/**
	 * Rédéfinition de {@link Convertisseur#convertir(GestionnaireErreur)}
	 * Convertit le fichier {@link Convertisseur#source} au format interne
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		tempsExecution = System.currentTimeMillis();
		boolean retour=true;
		nbCars = 0;
		nbMots = 0;
		nbPhrases = 0;
		try
		{
			gest.afficheMessage("** Ouverture du fichier source: " + source + " ...",Nat.LOG_VERBEUX);
			//RandomAccessFile raf = new RandomAccessFile(source, "r");
			BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(source),sourceEncoding));
			gest.afficheMessage("ok\n** Conversion du fichier source ...",Nat.LOG_NORMAL);
			BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cible),"UTF8"));

			//on met les entêtes au fichier xml
			fcible.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?>");
			fcible.write("\n<!DOCTYPE doc:doc SYSTEM \""+DTD +"\">");
			fcible.write("\n<doc:doc xmlns:doc=\"espaceDoc\">");
	
			String ligne;

			ligne =  raf.readLine();
			while ( ligne != null )
			{
				nbPhrases++;
				fcible.write("\n\t<phrase>\n");
				ligneLit(ligne, gest, fcible);
				fcible.write("\n\t</phrase>\n");
				ligne =  raf.readLine();
			}
			fcible.write("\n</doc:doc>");
			fcible.close();
			gest.afficheMessage("\nLe document contient " + nbPhrases +" paragraphes, " + nbMots + " mots et " + nbCars +" caractères.",Nat.LOG_VERBEUX);
			tempsExecution = System.currentTimeMillis() - tempsExecution;
			gest.afficheMessage("ok\n----Conversion terminée en " + tempsExecution + " msec.\n",Nat.LOG_SILENCIEUX);
			retour = true;
		}
		catch (java.io.IOException e)
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}
		catch (Exception e)  
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}		
		return retour;//retour;
	}
	/**
	 * Convertit une ligne littéraire au format interne
	 * @param ligne la ligne à convertir
	 * @param gest une instance de {@link GestionnaireErreur}
	 * @param fcible le BufferedWriter utilisé pour {@link Convertisseur#cible}
	 */
	protected void ligneLit(String ligne, GestionnaireErreur gest, BufferedWriter fcible)
	{
		int i=0;
		int j=0;
		String [] mots = null;
		//feinte pour pas s'emmm... avec les points de suspensions et les tab:
		ligne = ligne.replace("...","…");
		ligne = ligne.replace("\t",""+espace);
		ligne = ligne.replace("\n","");
		ligne = ligne.replace("\u00A0",""+espace); //espace insécable
		if (ligne != null && ligne.length()>0)
		{
			mots=ligne.split(""+espace);
			if (mots.length == 0) // si il n'y a qu'un seul mot dans la ligne
			{
				mots = new String[1];
				mots[0] = ligne;
			}
			//System.err.println("ligne:" + ligne + " mot0:" + mots[0]);
		}
		if ((mots != null) && !(mots.length==1 && mots[0] == ""+espace))// changer avec taille split:fait
		{
			try
			{
				fcible.write("\n\t\t<lit>");
				nbMots = nbMots + mots.length;
				while (i<mots.length)
				{
					j=0;
					//boolean trouve=false;
					boolean suivant = false;
					//int debutMot = 0;
					while (j<ponctuationDebut.length)
					{
						if (mots[i].startsWith(ponctuationDebut[j])||mots[i]==ponctuationDebut[j])
						{		//mots[i].length()-1		    	
							fcible.write("\n\t\t\t<ponctuation>" + mots[i].charAt(0) + "</ponctuation>");
							if (mots[i].length()>1)
							{
								mots[i] = mots[i].substring(1,mots[i].length());
								j=0;
							}
							else
							{
							//c'est fini, on passe au mot suivant
								suivant = true;
							}
							nbCars = nbCars + 1;
						}
						j++;
					}
					j=0;
					//trouve = false;
					if(!suivant)
					{
						ArrayList<String> ponctfin=  new ArrayList<String>();
						// on extrait les ponctuations de fin
						while (j<ponctuationFin.length)
						{
							if (mots[i].endsWith(ponctuationFin[j])||mots[i]==ponctuationFin[j])
							{			
								ponctfin.add("\n\t\t\t<ponctuation>" +mots[i].charAt(mots[i].length()-1) + "</ponctuation>");
								mots[i] = mots[i].substring(0,mots[i].length()-1);
								j=0;
							}
							else{j++;}
								/*if (mots[i].length()>1)
								{
									fcible.write("\n\t\t\t<mot>" + mots[i].substring(0,mots[i].length()-1).replace("&","&amp;").replace("<","&lt;") +"</mot>");
								}
								fcible.write("\n\t\t\t<ponctuation>" + mots[i].charAt(mots[i].length()-1) + "</ponctuation>");
								nbCars = nbCars + mots[i].length() + 1;
								trouve=true;*/
						}
						fcible.write("\n\t\t\t<mot>" + mots[i].replace("&","&amp;").replace("<","&lt;") + "</mot>");//\t\t\t<espace></espace>");
						int nbPonct = ponctfin.size();
						nbCars = nbCars + mots[i].length() + nbPonct;
						// on écrit les ponctuations si il y en a
						for(j=nbPonct-1;j>=0;j--){fcible.write(ponctfin.get(j));}	
					}
					i++;
				}
				i=0;
				fcible.write("\n\t\t</lit>\n");
			}
		catch (java.io.IOException e)
			{
			 gest.setException(e);
			 gest.gestionErreur();
			}
		}
	}
	/* ça sert plus ça
	public void toISO()
	{
		BufferedReader br = null;
		PrintWriter pw = null;
		try
		{
			//flux de lecture en UTF-8
			br = new BufferedReader(new InputStreamReader(new FileInputStream(source),"UTF-8"));
			//flux d'écriture en ISO (valeur par défaut)
			pw = new PrintWriter(new FileOutputStream(sourceEncode));
			String ligne;
			while((ligne = br.readLine())!=null)
			{
				pw.println(ligne);
			}
		}
		catch(Exception e){e.printStackTrace();}
		finally
		{
			try { pw.close();} catch( Throwable e ) {}
			try { br.close();} catch( Throwable e ) {}
		}
	}*/
	/**
	 * Spécifie l'encodage du fichier source
	 * @param se fichier source encoding
	 */
	public void setSourceEncoding(String se)
	{
		sourceEncoding = se;
	}
	
}