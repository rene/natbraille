/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import gestionnaires.GestionnaireErreur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import nat.Nat;

/**
 * Convertisseur Texte pour TAN
 * Supprime les coupures et les sauts de pages du document braille d'origine
 * @author bruno
 *
 */
public class ConvertisseurTan extends ConvertisseurTexte
{
    /**
	 * Constructeur
	 * <p>Par défaut, utilise l'encodage UTF-8</p>
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 */
	public ConvertisseurTan(String src, String tgt){super(src, tgt);}
	/**
	 * Constructeur
	 * @param src l'adresse du fichier source
	 * @param tgt l'adresse du fichier cible
	 * @param sEncoding encodage du fichier source
	 */
	public ConvertisseurTan(String src, String tgt,String sEncoding){super(src, tgt,sEncoding);}
	
	/**
	 * Constructeur
	 * <p>L'encodage utilisé par défaut est UTF-8 </p>
	 * @param src l'adresse du fichier source à convertir
	 * @param tgt l'adresse du fichier cible au format interne
	 * @param sep charactère séparateur de mot
	 */
	public ConvertisseurTan(String src, String tgt, char sep){super(src,tgt,sep);}
	/**
	 * Rédéfinition de {@link ConvertisseurTexte#convertir(GestionnaireErreur)}
	 * Convertit le fichier {@link ConvertisseurTexte#source} au format interne
	 * Le fichier d'entrée doit être en Braille UTF8, 
	 * les sauts de pages et les coupures sont supprimés
	 */
	@Override
	public boolean convertir(GestionnaireErreur gest)
	{
		tempsExecution = System.currentTimeMillis();
		boolean retour=true;
		nbCars = 0;
		nbMots = 0;
		nbPhrases = 0;
		try
		{
			gest.afficheMessage("** Ouverture du fichier source: " + source + " ...",Nat.LOG_VERBEUX);
			//RandomAccessFile raf = new RandomAccessFile(source, "r");
			BufferedReader raf = new BufferedReader(new InputStreamReader(new FileInputStream(source),sourceEncoding));
			gest.afficheMessage("ok\n** Conversion du fichier source ...",Nat.LOG_NORMAL);
			BufferedWriter fcible = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cible),"UTF8"));

			//on met les entêtes au fichier xml
			fcible.write("<?xml version=\"1.1\" encoding=\"UTF-8\"?>");
			fcible.write("\n<!DOCTYPE doc:doc SYSTEM \""+DTD +"\">");
			fcible.write("\n<doc:doc xmlns:doc=\"espaceDoc\">");
	
			String l;
			ArrayList<String>lignes = new ArrayList<String>();
			l =  raf.readLine();
			while (l != null)
			{
				//suppression des sauts de pages
				lignes.add(l.replaceAll(""+(char)12, ""));
				l =  raf.readLine();
			}
			for(int i = 0; i< lignes.size()-1;i++)
			{
				String ligne = lignes.get(i);
				if(ligne.endsWith(""+'\u2810') || 
					(ligne.endsWith(""+'\u2824')&&!ligne.endsWith(""+'\u2824'+'\u2824')))
				{
					String s = ligne.substring(0,ligne.length()-1)+lignes.get(i+1);
					lignes.set(i+1, s);
				}
				else
				{
					nbPhrases++;
					fcible.write("\n\t<phrase>\n");
					ligneLit(ligne, gest, fcible);
					fcible.write("\n\t</phrase>\n");
				}
			}
			//dernière phrase:
			nbPhrases++;
			fcible.write("\n\t<phrase>\n");
			ligneLit(lignes.get(lignes.size()-1), gest, fcible);
			fcible.write("\n\t</phrase>\n");
			
			fcible.write("\n</doc:doc>");
			fcible.close();
			gest.afficheMessage("\nLe document contient " + nbPhrases +" paragraphes, " + nbMots + " mots et " + nbCars +" caractères.",Nat.LOG_VERBEUX);
			tempsExecution = System.currentTimeMillis() - tempsExecution;
			gest.afficheMessage("ok\n----Conversion terminée en " + tempsExecution + " msec.\n",Nat.LOG_SILENCIEUX);
			retour = true;
		}
		catch (java.io.IOException e)
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}
		catch (Exception e)  
		{
			gest.setException(e);
			gest.gestionErreur();
			retour = false;
		}		
		return retour;
	}

}
