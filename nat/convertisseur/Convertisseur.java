/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package nat.convertisseur;

import nat.ConfigNat;
import gestionnaires.GestionnaireErreur;

/**
 * Classe abstraite décrivant ce qu'est un Convertisseur
 * Un convertisseur est en général utilisé en début de scénario afin de transformer 
 * un format donné en format interne
 * @author bruno
 *
 */
public abstract class Convertisseur
{
	/** L'adresse de la source à convertir */
	protected String source;
	/** L'adresse du fichier cible au format interne de nat */
	protected String cible;
	/** Nombre de caractères de la source */
	protected int nbCars;
	/** Nombre de mots de la source */
	protected int nbMots;
	/** Nombre de phrases de la source */
	protected int nbPhrases;
	/** Temps d'exécution en millisecondes de la conversion */
	protected long tempsExecution;
	/** Adresse de la DTD utilisée pour le format interne */
	protected String DTD = ConfigNat.getCurrentConfig().getDTD();
	
	/**
	 * Constructeur
	 * @param src l'adresse de la source
	 * @param c l'adresse de la cible
	 */
	public Convertisseur(String src, String c)
	{
		source=src;
		cible=c;
	}
	/**
	 * Méthode d'accès, modifie l'attribut {@link Convertisseur#source}
	 * @param src la nouvelle valeur de {@link Convertisseur#source}
	 */
	public void setSource(String src){source=src;}
	/**
	 * Méthode d'accès, modifie l'attribut {@link Convertisseur#DTD}
	 * @param laDTD la nouvelle valeur de {@link Convertisseur#DTD}
	 */ 
	public void setDTD(String laDTD){DTD=laDTD;}
	/**
	 * Méthode d'accès, modifie l'attribut {@link Convertisseur#cible}
	 * @param c la nouvelle valeur de {@link Convertisseur#cible}
	 */
	public void setCible(String c){cible=c;}
	
	/* inutilisé
	public void setFormat(int f){}
	*/
	/** 
	 * Renvoie la valeur de {@link Convertisseur#tempsExecution}
	 * @return {@link Convertisseur#tempsExecution}
	 */
	public long donneTempsExecution(){return tempsExecution;}
	/**
	 * Méthode réalisant la conversion, à redéfinir par les héritiers
	 * @param gest une instance de {@link GestionnaireErreur}
	 * @return true si la conversion s'est bien passée, false sinon
	 */
	public abstract boolean convertir(GestionnaireErreur gest);
}
