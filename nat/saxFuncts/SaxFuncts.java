/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.saxFuncts;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Cette classe est destinée à fournir une version modifiée du translate de xsl pour les grosses opérations
 * de conversion entre tables
 * @author bruno
 *
 */
public class SaxFuncts
{
	
	/**
	 * Cette méthode fournie une version modifiée du translate de xsl pour les grosses opérations
	 * de conversion entre tables braille
	 * @param template la chaine à convertir
	 * @param in la chaine de conversion d'entrée
	 * @param out la chaine de conversion de sortie
	 * @return la chaine convertie
	 */
	
	public static String translate(String template, String in, String out)
	{
		Map<String,String> tokens = new HashMap<String,String>();
		//String template = "cat really needs some beverage.";
	
		// Create pattern of the format "%(cat|beverage)%"
		String patternString ="(";
		for(int i=0;i<in.length();i++)
		{
			String s = ""+in.charAt(i);
			tokens.put(s, ""+out.charAt(i));
			patternString = patternString + s + "|";
		}
		patternString = patternString.substring(0, patternString.length()-1) + ")";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(template);
		//System.out.println(patternString);
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
		    matcher.appendReplacement(sb, tokens.get(matcher.group(1)));
		}
		matcher.appendTail(sb);	
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	/**
	 * Test methode
	 * @param arg param strings
	 */
	public static void main(String arg[])
	{
		/*ArrayList<String> t1 = new ArrayList<String>();
		ArrayList<String> t2 = new ArrayList<String>();
		t1.add("cat");
		t2.add("felix");
		t1.add("beverage");
		t2.add("milk");
		new BrailleTranslate(t1,t2);*/
		SaxFuncts.translate("Salut les copains, ça roule bien ici?","abc", "bBC");
	}

}
