/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.ListCellRenderer;
import javax.swing.JList;
import javax.swing.ImageIcon;

/**
 * Classe décrivant le rendu du JComboBox contenant les tables Brailles possibles
 * @see ConfGeneral
 * @author Bruno
 *
 */
public class BrailleTableComboBoxRenderer extends JLabel implements ListCellRenderer
{ 
    /** Pour la sérialisation, non utilisé */
	private static final long serialVersionUID = 1L;
	/** ImageIcon pour les configutrations système */
	private ImageIcon iconSys = new ImageIcon("ui/icon/system-run.png");
	/** ImageIcon pour les configurations de l'utilisateur */
    private ImageIcon iconUsr = new ImageIcon("ui/icon/gtk-edit.png");
    /** Constructeur */
    public BrailleTableComboBoxRenderer()
    {
    	setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
    }
    /**
     * Renvoie le rendu pour une configuration donnée
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
     */
    public Component getListCellRendererComponent(
						  JList list,
						  Object value,
						  int index,
						  boolean isSelected,
						  boolean cellHasFocus)
    {
		BrailleTableListItem btli = (BrailleTableListItem) value;
		if (btli != null)
		{
		    if (isSelected)
		    {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
		    }
		    else
		    {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
		    }
		    
		    ImageIcon icon = (btli.getIsSystem())?iconSys:iconUsr;
		    setIcon(icon);
		    String accessNom="";
		    String accessDesc="";
		    if(btli.getIsSystem())
		    {
		    	accessNom = "Table Braille système " + btli.getName();
		    	accessDesc = "Table braille système " + btli.getName() + " (non modifiable)";
		    }
		    else
		    {
		    	accessNom = "Table Braille personnelle " + btli.getName();
		    	accessDesc = "Table braille personnelle " + btli.getName() + " (modifiable)";
		    }
		    getAccessibleContext().setAccessibleDescription(accessDesc);
		    getAccessibleContext().setAccessibleName(accessNom);
		    setToolTipText(accessDesc);
		    
		    if (icon != null) {	setText(btli.getName());}
		}
		return this;
    }
    
}
