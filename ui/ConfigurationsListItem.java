/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import java.io.FileInputStream;
import java.io.IOException;
//import java.io.InputStreamReader;
import java.util.Properties;

import nat.OptNames;
/**
 * Item de liste pour le JComboBox de la fenêtre principale de NAT contenant 
 * les configuration possibles
 * @see FenetrePrinc
 * @author Vivien, Bruno
 *
 */
public class ConfigurationsListItem
{
    /** adresse du fichier de configuration */
    private String  filename = null;
    /** Nom de la configuration */
    private String  name = null;
    /** Informations sur la configuration */
    private String  infos = null;
    /** Vrai si la configuration est une configuration système */
    private boolean isSystem;
    /** vrai si la configuration est valide */
    private boolean isValid;
    /**
     * Constructeur
     * @param in_filename adresse du fichier de configuration
     */
    public ConfigurationsListItem(String in_filename)
    {
		Properties conf = new Properties();
		try
		{
		    conf.load(new FileInputStream(in_filename));
		    isSystem = new Boolean(conf.getProperty(OptNames.fi_is_sys_config,"false")).booleanValue();
		    infos    = conf.getProperty(OptNames.fi_infos);
		    name     = conf.getProperty(OptNames.fi_name);
		    filename = in_filename;
		    if ((filename != null)&&(name != null)){isValid = true;}
		    else {isValid = false;}
		}
		catch (IOException ioe) {System.err.println("Exception while trying to parse config file "+in_filename);}
    }
    /**
     * Compare deux configurations, une (<code>cli</code>) représentée par une instance de ConfigurationsListItem
     * et l'autre par le nom de la configuration représentée par <code>this</code>
     * @param cli l'instance de {@link ConfigurationsListItem}
     * @return true si {@link #filename} est le même que <code>cli</code> 
     */
    public boolean equals(ConfigurationsListItem cli){return (filename.equals(cli));}
    /**
     * Méthode d'accès en lecture à {@link #isValid}
     * @return {@link #isValid}
     */
    public boolean getIsValid(){return isValid;}
    /**
     * Méthode d'accès en lecture à {@link #filename}
     * @return {@link #filename}
     */
    public String getFilename(){return filename;}
    /**
     * Méthode d'accès en lecture à {@link #name}
     * @return {@link #name}
     */
    public String getName(){return name;}
    /**
     * Méthode d'accès en lecture à {@link #infos}
     * @return {@link #infos}
     */
    public String getInfos(){return infos;}
    /**
     * Méthode d'accès en lecture à {@link #isSystem}
     * @return {@link #isSystem}
     */
    public boolean getIsSystem(){return isSystem;}
    
}
