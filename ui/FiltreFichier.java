/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import java.io.File;
import javax.swing.filechooser.FileFilter;
/**
 * Classe de gestion des filtres pour les fichiers utilisés dans nat en fonction de leur extension
 * d'après http://brassens.upmf-grenoble.fr/IMSS/dciss/Enseignements/PSR/Prog/Java/dialogueFichier.htm
 * @author bruno
 *
 */
public class FiltreFichier extends FileFilter 
{
	/** Tableau des suffixes (extensions) des fichiers */
	private String []lesSuffixes;
	/** Descriptions des suffixes de {@link #lesSuffixes}*/
	private String  laDescription;
	/**
	 * Constructeur
	 * <p>Crée un filtre avec la description <code>laDescription</code> pour les suffixes 
	 * <code>lesSuffixes</code></p>
	 * @param lesSuff liste des suffixes
	 * @param laDesc description pour les suffixes
	 */
	public FiltreFichier(String []lesSuff, String laDesc)
	{
		lesSuffixes = lesSuff;
        laDescription = laDesc;
    }
	/**
	 * Implémentation de javax.swing.filechooser.FileFilter#getDescription()
	 * <p>Méthode d'accès renvoyant la description du filtre</p>
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	@Override
	public String getDescription(){return laDescription;}
	/**
	 * Renvoie vrai si le suffixe <code>suffixe</code> fait partie du tableau {@link #lesSuffixes}
	 * @param suffixe le suffixe (extension) à rechercher
	 * @return true si le suffixe est dans {@link #lesSuffixes}, false sinon
	 */
	boolean appartient(String suffixe )
	{
		boolean retour = false;
		for( int i = 0; i<lesSuffixes.length; ++i)
		{
			if(suffixe.equals(lesSuffixes[i])){retour = true;}
		}
		return retour;
	}
	/**
	 * Implémentation de javax.swing.filechooser.FileFilter#accept(java.io.File)
	 * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
	 * @return true si le fichier <code>f</code> correspond au filtre de l'instance
	 */
	@Override
	public boolean accept(File f)
	{
		boolean retour = false;
		if (f.isDirectory()){retour = true;}
		else
		{
			String suffixe = null;
			String s = f.getName();
			int i = s.lastIndexOf('.');
			if (i > 0 &&  i < s.length() - 1){suffixe = s.substring(i+1).toLowerCase();}
			retour = suffixe != null && appartient(suffixe);
		}
		return retour;
	 }
}