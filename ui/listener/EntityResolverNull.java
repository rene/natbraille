/*
 * Trace assistant
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ui.listener;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Resolver qui ne resolve rien du tout;
 * Permet de se passer de dtd pour les parsage de documents
 * @author bruno
 *
 */
public class EntityResolverNull implements EntityResolver
{

	/**
	 * Permet de se passer de dtd
	 * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
	 */
	@Override
	public InputSource resolveEntity(String publicId, String systemId)throws SAXException, IOException
	{
		String empty = "";
		ByteArrayInputStream bais = new ByteArrayInputStream(empty.getBytes());
		//System.out.println("resolveEntity:" + publicId + "|" + systemId);
		return new InputSource(bais);
	}

}
