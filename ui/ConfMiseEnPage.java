/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import nat.ConfigNat;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


/**
 * Onglet de configuration de la mise en page
 * @see OngletConf
 * @author bruno
 *
 */
public class ConfMiseEnPage extends OngletConf implements ItemListener, ChangeListener
{
	/** pour la sérialisation (non utilisé */
	private static final long serialVersionUID = 1L;
	
	//private JSpinner jsRetraitDebLigne;
	/** JCheckBox pour produire un saut de page */
	private JCheckBox jchMinPageBreak = new JCheckBox("Générer un saut de page à partir de");
	/** JSpinner minimum pour produire un saut de page */
	private JSpinner jsMinPageBreak;
	/** JSpinner minimum pour produire 3 lignes vides */
	private JSpinner jsMin3L;
	/** JSpinner minimum pour produire 2 lignes vides */
	private JSpinner jsMin2L;
	/** JSpinner minimum pour produire 1 ligne vide */
	private JSpinner jsMin1L;
	/** JSpinner longueur de la ligne braille */
	private JSpinner jsLongLigne;
	/** JLabel longueur de la ligne braille */
	private JLabel lLongLigne = new JLabel("Nombre de caractères par ligne: ");
	/** JSpinner nombre de lignes par page */
	private JSpinner jsNbLigne;
	/** JLabel Nombre de lignes par page braille*/
	private JLabel lNbLigne = new JLabel("Nombre de lignes par pages: ");
	/** JCheckBox ajouter un saut de page */
	private JCheckBox jchbSautFin = new JCheckBox("Ajouter un saut de page en fin de document");
	/** JCheckBox linéariser les structures de type table */
	private JCheckBox jchbLineariseTable = new JCheckBox("Toujours linéariser les structures de type table");
	/** JSpinner nombre minimum de cellule pour rendre une table en 2D */
	private JSpinner jsMinCellLin;
	/** JLabel nombre minimum de cellule pour rendre une table en 2D */
	private JLabel ljsMinLin = new JLabel("Nombre de cellules requises pour essayer un rendu 2D:");
	/** JComboBox contenant les styles de numérotations possibles*/
	private JComboBox jcbNumerotation;
	/** Tableau des numérotations possibles */
	private final String [] tabNumPossible = {"aucune numérotation","en haut seul sur une ligne","en haut sur une ligne partagée","en bas seul sur une ligne","en bas sur une ligne partagée"};
	/** Constante d'accès représentant l'indice de "Aucune Numérotation" dans {@link #tabNumPossible}*/
	private final int NUM_AUCUN = 0;
	/** Constante d'accès représentant l'indice de "en haut sur une ligne partagée" dans {@link #tabNumPossible}*/
	private final int NUM_HB = 2;
	/** Constante d'accès représentant l'indice de "en haut seul sur une ligne" dans {@link #tabNumPossible}*/
	private final int NUM_HS = 1;
	/** Constante d'accès représentant l'indice de "en bas sur une ligne partagée" dans {@link #tabNumPossible}*/
	private final int NUM_BB = 4;
	/** Constante d'accès représentant l'indice de "en bas sur une ligne" dans {@link #tabNumPossible}*/
	private final int NUM_BS = 3;
	/** JCheckBox numerotation premiere page */
	private JCheckBox jchbNumeroteFirst = new JCheckBox("Numéroter la première page");
	/** JComboBox contenant les styles de gestion des lignes vides possibles*/
	private JComboBox jcbLV;
	/** Tableau des gestions des lignes vides possibles */
	private final String [] tabLVPossible = {"comme dans le document d'origine","personnalisée","aucune ligne vide","norme braille (aérée)","norme braille (compacte)"};
	/** Panneau masquable contenant les options personnalisées pour la gestion des lignes vides */
	private JPanel panLVPerso = new JPanel(); 
	/** Label pour le JSpinner 1 ligne en sortie*/
	private JLabel ljs1 = new JLabel(" lignes vides en produisent 1 en sortie");
	/** Label pour le JSpinner 2 lignes en sortie*/
	private JLabel ljs2 = new JLabel(" lignes vides en produisent 2 en sortie");
	/** Label pour le JSpinner 3 lignes en sortie*/
	private JLabel ljs3 = new JLabel(" lignes vides ou plus en produisent 3 en sortie");
	/** Label pour le JSpinner saut de page*/
	private JLabel ljspb = new JLabel(" lignes vides");

	
	/**
	 * Constructeur de l'onglet Mise en page
	 *
	 */
	public ConfMiseEnPage()
	{
		super();
		getAccessibleContext().setAccessibleDescription("Activez cet onglet pour afficher les options de mise en page");
		getAccessibleContext().setAccessibleName("Onglet contenant les options de mise en page");

		/**********
		 * Préparation des composants
		 */
		
		/* Dimensions de la page */
		jsLongLigne = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getLongueurLigne(), 10, 1000, 1));
		jsLongLigne.getAccessibleContext().setAccessibleName("Liste de valeurs pour le nombre de caractères par ligne braille");
		jsLongLigne.getAccessibleContext().setAccessibleDescription("Sélectionnez avec les flèches le nombre de caractères par ligne");
		jsLongLigne.setToolTipText("Sélectionnez la longueur de la ligne souhaitée en nombre de caractères (Alt+b)");
		lLongLigne.setLabelFor(jsLongLigne);
		lLongLigne.setDisplayedMnemonic('b');
		
		jsNbLigne = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getNbLigne(), 10, 1000, 1));
		jsNbLigne.getAccessibleContext().setAccessibleName("Liste de valeurs pour le nombre de lignes par pages");
		jsNbLigne.getAccessibleContext().setAccessibleDescription("Sélectionnez avec les flèches le nombre de lignes");
		jsNbLigne.setToolTipText("Sélectionnez le nombre de lignes brailles par page (Alt+m)");
		lNbLigne.setLabelFor(jsNbLigne);
		lNbLigne.setDisplayedMnemonic('m');
		
		/* Numérotation */
		jcbNumerotation = new JComboBox(tabNumPossible);
		jcbNumerotation.getAccessibleContext().setAccessibleName("Liste à choix multiple pour la numérotation");
		jcbNumerotation.getAccessibleContext().setAccessibleDescription("Choississez un type de numérotation avec les flêches");
		jcbNumerotation.setToolTipText("Choississez un type de numérotation (Alt+p)");
		jcbNumerotation.addItemListener(this);
		
		JLabel lNumerotation = new JLabel("Numérotation des pages:");
		lNumerotation.setLabelFor(jcbNumerotation);
		lNumerotation.setDisplayedMnemonic('p');
		
		jchbNumeroteFirst.getAccessibleContext().setAccessibleName("Pour numéroter la première page");
		jchbNumeroteFirst.setToolTipText ("Cochez pour forcer la numérotation de la première page (alt+e)");
		jchbNumeroteFirst.getAccessibleContext().setAccessibleDescription(getToolTipText());
		jchbNumeroteFirst.setSelected(ConfigNat.getCurrentConfig().getNumeroteFirst());
		jchbNumeroteFirst.setMnemonic('e');
		
		String numStyle = ConfigNat.getCurrentConfig().getNumerotation();
		if(numStyle.equals("'nn'")){jcbNumerotation.setSelectedIndex(NUM_AUCUN);}
		else if(numStyle.equals("'hb'")){jcbNumerotation.setSelectedIndex(NUM_HB);}
		else if(numStyle.equals("'hs'")){jcbNumerotation.setSelectedIndex(NUM_HS);}
		else if(numStyle.equals("'bs'")){jcbNumerotation.setSelectedIndex(NUM_BS);}
		else if(numStyle.equals("'bb'")){jcbNumerotation.setSelectedIndex(NUM_BB);}
		
		/* lignes vides */
		
	    //gestion perso
		ljs1.setDisplayedMnemonic('v');
		//ljs2.setDisplayedMnemonic('v');
		//ljs3.setDisplayedMnemonic('p');
		
		jsMin1L = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLigne1(), 1, 40, 1));
	    ljs1.setLabelFor(jsMin1L);
	    jsMin1L.getAccessibleContext().setAccessibleName("Liste déroulante nombre minimum de lignes vides pour une ligne vide");
	    jsMin1L.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre de lignes vides à partir duquel une ligne vide sera produite");
	    jsMin1L.setToolTipText("nombre de lignes vides à partir duquel une ligne vide sera produite (Alt+v)");
	    jsMin1L.addChangeListener(this);
	    
	    jsMin2L = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLigne2(), 2, 40, 1));
	    ljs2.setLabelFor(jsMin2L);
	    jsMin2L.getAccessibleContext().setAccessibleName("Liste déroulante nombre minimum de lignes vides pour générer deux lignes vides");
	    jsMin2L.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre de lignes vides à partir duquel deux lignes vides seront produites");
	    jsMin2L.setToolTipText("nombre de lignes vides à partir duquel deux lignes vides seront produites (Alt+v puis tab)");
	    jsMin2L.addChangeListener(this);
	    
	    jsMin3L = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLigne3(), 3, 40, 1));
	    ljs3.setLabelFor(jsMin3L);
	    jsMin3L.getAccessibleContext().setAccessibleName("Liste déroulante nombre minimum de lignes vides pour générer trois lignes vides");
	    jsMin3L.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre de lignes vides à partir duquel trois lignes vides seront produites");
	    jsMin3L.setToolTipText("nombre de lignes vides à partir duquel trois lignes vides seront produites (Alt+v puis deux tab)");
	    jsMin3L.addChangeListener(this);
	    
	    jchMinPageBreak.setSelected(ConfigNat.getCurrentConfig().getGeneratePB());
	    jchMinPageBreak.getAccessibleContext().setAccessibleName("Générer un saut de page à partir d'un certain nombre de lignes vides");
	    jchMinPageBreak.setToolTipText("Sélectionner cette option pour générer un saut de page à partir d'un certain nombre de lignes vides (Alt+u)");
	    jchMinPageBreak.getAccessibleContext().setAccessibleDescription(getToolTipText());
	    jchMinPageBreak.setMnemonic('u');
	    jchMinPageBreak.addItemListener(this);
	    
	    jsMinPageBreak = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepMinLignePB(), 4, 40, 1));
	    ljspb.setLabelFor(jsMinPageBreak);
	    jsMinPageBreak.getAccessibleContext().setAccessibleName("Liste déroulante nombre minimum de lignes vides pour générer un saut de page");
	    jsMinPageBreak.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre de lignes vides à partir duquel un saut de page sera produit");
	    jsMinPageBreak.setToolTipText("nombre de lignes vides à partir duquel un saut de page sera produit (Alt+g puis tab)");
	    jsMinPageBreak.addChangeListener(this);
	    jsMinPageBreak.setEnabled(jchMinPageBreak.isSelected());
	    
	    //option des lv
		jcbLV = new JComboBox(tabLVPossible);
		jcbLV.getAccessibleContext().setAccessibleName("Liste à choix multiple pour la gestion des lignes vides");
		jcbLV.getAccessibleContext().setAccessibleDescription("Choississez un type de gestion des lignes vides; le choix \"personnalisée\" fait apparaître des options supplémentaires");
		jcbLV.setToolTipText("Choississez un mode de gestion des lignes vides (Alt+l)");
		//jcbLV.getAccessibleContext().setAccessibleDescription(getToolTipText());
		jcbLV.addItemListener(this);
		jcbLV.setSelectedIndex(ConfigNat.getCurrentConfig().getMepModelignes());
		
		JLabel lLignesVides = new JLabel("Gestion des lignes vides:");
		lLignesVides.setDisplayedMnemonic('l');
		lLignesVides.setLabelFor(jcbLV);
	    
	    jchbSautFin.setSelected(ConfigNat.getCurrentConfig().getSautPageFin());
	    jchbSautFin.getAccessibleContext().setAccessibleName("Case à cocher ajouter un saut de page en fin de document");
	    jchbSautFin.getAccessibleContext().setAccessibleDescription("Sélectionner cette option pour ajouter un saut de page à la fin du document");
	    jchbSautFin.setToolTipText("Sélectionner cette option pour ajouter un saut de page à la fin du document (Alt+f)");
	    jchbSautFin.setMnemonic('f');
	    
	    /* Options de mise en forme */
	    /*JLabel ljsRetraitDebLigne = new JLabel("Nombre d'espaces en début de paragraphe:");
	    ljsRetraitDebLigne.setDisplayedMnemonic('e');
		
	    jsRetraitDebLigne = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMepRetraitPar(), 0, 10, 1));
	    ljsRetraitDebLigne.setLabelFor(jsRetraitDebLigne);
	    jsRetraitDebLigne.getAccessibleContext().setAccessibleName("Liste déroulante taille du retrait en début de paragraphe");
	    jsRetraitDebLigne.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre d'espace à insérer en début de paragraphe");
	    jsRetraitDebLigne.setToolTipText("nombre d'espaces à insérer en début de paragraphe");
	    */
				
	    jchbLineariseTable.setSelected(ConfigNat.getCurrentConfig().getLineariseTable());
	    jchbLineariseTable.getAccessibleContext().setAccessibleName("Case à cocher linéariser les tables");
	    jchbLineariseTable.getAccessibleContext().setAccessibleDescription("Sélectionner cette option pour toujours linéariser les tables");
	    jchbLineariseTable.setToolTipText("Sélectionner cette option pour toujours linéariser les tables (Alt+t)");
	    jchbLineariseTable.setMnemonic('t');
	    jchbLineariseTable.addItemListener(this);
	    
	    //A mettre après jchbLineariseTable.addItemListener(this);
	    ljsMinLin.setDisplayedMnemonic('y');
	    ljsMinLin.setEnabled(!jchbLineariseTable.isSelected());
		// A mettre après jchbLineariseTable.addItemListener(this);
		jsMinCellLin = new JSpinner(new SpinnerNumberModel(ConfigNat.getCurrentConfig().getMinCellLin(), 0, 30, 1));
		ljsMinLin.setLabelFor(jsMinCellLin);
		jsMinCellLin.getAccessibleContext().setAccessibleName("Liste déroulante nombre minimal de cellules pour essayer un affichage en 2D");
		jsMinCellLin.getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le nombre minimal de cellules pour essayer un affichage en 2D (sinon, linéarisation)");
		jsMinCellLin.setToolTipText("nombre minimal de cellules pour essayer un affichage en 2D (Alt+y)");
		jsMinCellLin.setEnabled(!jchbLineariseTable.isSelected());
	    
		/*********
		 * Mise en page
		 */
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		JPanel panTr = new JPanel();
		panTr.setLayout(gbl);
		
		JLabel titre = new JLabel("<html><h3>Paramétrage de la mise en page</h3></html>");	
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=3;
		gbl.setConstraints(titre, gbc);
		panTr.add(titre);
		
		JLabel titre2 = new JLabel("<html><h4>Dimensions</h4></html>");	
		gbc.gridy++;
		gbl.setConstraints(titre2, gbc);
		panTr.add(titre2);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth=2;
		gbl.setConstraints(lLongLigne, gbc);
		panTr.add(lLongLigne);
		
		gbc.gridx=gbc.gridx+2;
		gbc.gridwidth=1;
		gbl.setConstraints(jsLongLigne, gbc);
		panTr.add(jsLongLigne);
		
		gbc.gridx=1;
		gbc.gridy++;
		gbc.gridwidth=2;
		gbl.setConstraints(lNbLigne, gbc);
		panTr.add(lNbLigne);
		
		gbc.gridx=gbc.gridx+2;
		gbc.gridwidth=1;
		gbl.setConstraints(jsNbLigne, gbc);
		panTr.add(jsNbLigne);
		
		//Numérotation
		gbc.gridwidth=4;
		gbc.gridx=0;
		gbc.gridy++;
		gbl.setConstraints(lNumerotation, gbc);
		panTr.add(lNumerotation);
		
		gbc.gridwidth=4;
		gbc.gridx=2;
		gbl.setConstraints(jcbNumerotation, gbc);
		panTr.add(jcbNumerotation);
		
		gbc.gridwidth=3;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(jchbNumeroteFirst, gbc);
		panTr.add(jchbNumeroteFirst);
		
		//lignes vides
		gbc.gridwidth=2;
		gbc.gridx=0;
		gbc.gridy++;
		gbl.setConstraints(lLignesVides, gbc);
		panTr.add(lLignesVides);
		
		gbc.gridwidth=2;
		gbc.gridx=2;
		gbl.setConstraints(jcbLV, gbc);
		panTr.add(jcbLV);
		
		GridBagConstraints gbcLV = new GridBagConstraints();
		//gbcLV.insets = new Insets(3,3,3,3);
		GridBagLayout glLV = new GridBagLayout();
		
		panLVPerso.setLayout(glLV);
		
		gbcLV.gridx = 0;
		gbcLV.gridy = 0;
		gbcLV.gridwidth=1;
		panLVPerso.add(jsMin1L,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(jsMin2L,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(jsMin3L,gbcLV);
		
		gbcLV.anchor = GridBagConstraints.WEST;
		gbcLV.gridy=0;
		gbcLV.gridx++;
		gbcLV.gridwidth=2;
		panLVPerso.add(ljs1,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(ljs2,gbcLV);
		gbcLV.gridy++;
		panLVPerso.add(ljs3,gbcLV);
		
		gbc.gridwidth=3;
		gbc.gridheight=3;
		gbc.gridy++;
		gbc.gridx=1;
		gbl.setConstraints(panLVPerso, gbc);
		panTr.add(panLVPerso);
		
		//panneau pour la ligne page break
		GridBagConstraints gbcMPB = new GridBagConstraints();
		GridBagLayout glMPB = new GridBagLayout();
		JPanel pMPB = new JPanel(glMPB);
		gbcMPB.gridx=1;
		gbcMPB.gridy=1;
		
		glMPB.setConstraints(jchMinPageBreak,gbcMPB);
		pMPB.add(jchMinPageBreak);
		gbcMPB.gridx++;
		glMPB.setConstraints(jsMinPageBreak,gbcMPB);
		pMPB.add(jsMinPageBreak);
		gbcMPB.gridx++;
		glMPB.setConstraints(ljspb,gbcMPB);
		pMPB.add(ljspb);
		
		gbc.gridwidth=2;
		gbc.gridheight=1;
		gbc.gridy +=4;
		gbc.gridx=1;
		
		panTr.add(pMPB,gbc);
		
		//mise en forme
		JLabel titre4 = new JLabel("<html><h4>Tableaux, matrices et structures 2D</h4></html>");	
		gbc.gridwidth=3;
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridheight=1;
		gbl.setConstraints(titre4, gbc);
		panTr.add(titre4);
		
		/*gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth=2;
		gbl.setConstraints(ljsRetraitDebLigne, gbc);
		panTr.add(ljsRetraitDebLigne);
		
		gbc.gridx=gbc.gridx+2;
		gbc.gridwidth=1;
		gbl.setConstraints(jsRetraitDebLigne, gbc);
		panTr.add(jsRetraitDebLigne);*/
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jchbLineariseTable, gbc);
		panTr.add(jchbLineariseTable);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=2;
		gbl.setConstraints(ljsMinLin, gbc);
		panTr.add(ljsMinLin);
		gbc.gridwidth=1;
		gbc.gridx=3;
		gbl.setConstraints(jsMinCellLin, gbc);
		panTr.add(jsMinCellLin);
		
		/*gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jchbSautFin, gbc);
		panTr.add(jchbSautFin);*/
		
		setLayout(new BorderLayout());
		add(panTr, BorderLayout.NORTH);
		
		//MAJ 
		changeLignes();
	}	

	/**
	 * Méthode redéfinie de ItemListener
	 * Masque les éléments utilisés dans la linéarisation suivant l'état de jchbLineariseTable
	 * Appel changeLignes() si la source est le combobox des lignes vides
	 * @param ie l'instance d'Itemevent
	 * @see #changeLignes()
	 */
	public void itemStateChanged(ItemEvent ie) 
	{
		if(ie.getSource()==jcbLV){changeLignes();}
		
		//pour la linéarisation
		if(ie.getSource()==jchbLineariseTable)
		{
			if(!jchbLineariseTable.isSelected())
			{
				jsMinCellLin.setEnabled(true);
				ljsMinLin.setEnabled(true);
			}
			else
			{
				jsMinCellLin.setEnabled(false);
				ljsMinLin.setEnabled(false);
			}
		}
		
		// pour la numerotation des pages
		if(ie.getSource()==jcbNumerotation)
		{
			jchbNumeroteFirst.setEnabled(!(jcbNumerotation.getSelectedIndex()==NUM_AUCUN));
		}
		
		// pour la génération de sauts de pages
		if (ie.getSource()==jchMinPageBreak)
		{
			jsMinPageBreak.setEnabled(jchMinPageBreak.isSelected());
		}
	}
	
	/**
	 * Active ou non les options avancées conernant les lignes vides, suivant que personnalisé est choisi ou non
	 *
	 */
	private void changeLignes()
	{
		if(jcbLV.getSelectedIndex()!=1)
		{
			jsMin1L.setEnabled(false);
			ljs1.setEnabled(false);
			jsMin2L.setEnabled(false);
			ljs2.setEnabled(false);
			jsMin3L.setEnabled(false);
			ljs3.setEnabled(false);
		}
		else
		{
			jsMin1L.setEnabled(true);
			ljs1.setEnabled(true);
			jsMin2L.setEnabled(true);
			ljs2.setEnabled(true);
			jsMin3L.setEnabled(true);
			ljs3.setEnabled(true);
		}
	}
	
	/**
	 * Redéfinie de ChangeListener
	 * Vérifie la cohérence des valeurs des JSpinner des lignes vides
	 * @param ce ChangeEvent généré
	 */
	public void stateChanged(ChangeEvent ce)
	{
		int v1 = ((Integer)jsMin1L.getValue()).intValue();
		int v2 = ((Integer)jsMin2L.getValue()).intValue();
		int v3 = ((Integer)jsMin3L.getValue()).intValue();
		int vpb = ((Integer)jsMinPageBreak.getValue()).intValue();
		
		Object source = ce.getSource();
		
		if (source == jsMin1L)
			{if(!(v1<v2)){jsMin2L.setValue(new Integer(v1+1));}}
		if (source == jsMin1L || source == jsMin2L)
			{if(!(v2<v3)){jsMin3L.setValue(new Integer(v2+1));}}
		if (source == jsMin1L || source == jsMin2L || source == jsMin3L)
			{if(!(v3<vpb)){jsMinPageBreak.setValue(new Integer(v3+1));}}
		
		if(v1>1)
		{
			if (source == jsMinPageBreak)
				{if(!(v3<vpb)){jsMin3L.setValue(new Integer(vpb-1));}}
			if (source == jsMinPageBreak || source == jsMin3L)
				{if(!(v2<v3)){jsMin2L.setValue(new Integer(v3-1));}}
			if (source == jsMinPageBreak || source == jsMin3L || source == jsMin2L)
				{if(!(v1<v2)){jsMin1L.setValue(new Integer(v2-1));}}
			
		}
	}
	
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer()
	{
		boolean retour = true;
		ConfigNat.getCurrentConfig().setMepModelignes(jcbLV.getSelectedIndex());
		ConfigNat.getCurrentConfig().setMepMinLigne1(((Integer)jsMin1L.getValue()).intValue());
		ConfigNat.getCurrentConfig().setMepMinLigne2(((Integer)jsMin2L.getValue()).intValue());
		ConfigNat.getCurrentConfig().setMepMinLigne3(((Integer)jsMin3L.getValue()).intValue());
		ConfigNat.getCurrentConfig().setMepMinLignePB(((Integer)jsMinPageBreak.getValue()).intValue());
		ConfigNat.getCurrentConfig().setGeneratePB(jchMinPageBreak.isSelected());
		ConfigNat.getCurrentConfig().setLongueurLigne(((Integer)jsLongLigne.getValue()).intValue());
		ConfigNat.getCurrentConfig().setNbLigne(((Integer)jsNbLigne.getValue()).intValue());
		ConfigNat.getCurrentConfig().setSautPageFin(jchbSautFin.isSelected());
		//ConfigNat.getCurrentConfig().setMepRetraitPar(((Integer)jsRetraitDebLigne.getValue()).intValue());
		ConfigNat.getCurrentConfig().setLineariseTable(jchbLineariseTable.isSelected());
		//ConfigNat.getCurrentConfig().sauvegarder();
		ConfigNat.getCurrentConfig().setMinCellLin(((Integer)jsMinCellLin.getValue()).intValue());
		switch (jcbNumerotation.getSelectedIndex())
		{
			case NUM_AUCUN:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'nn'");
				break;
			}
			case NUM_HS:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'hs'");
				break;
			}
			case NUM_HB:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'hb'");
				break;
			}
			case NUM_BS:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'bs'");
				break;
			}
			case NUM_BB:
			{
				ConfigNat.getCurrentConfig().setNumerotation("'bb'");
				break;
			}	
		}
		ConfigNat.getCurrentConfig().setNumeroteFirst(jchbNumeroteFirst.isSelected());
		ConfigNat.getCurrentConfig().sauvegarder();
		return retour;
	}
}
