/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import nat.ConfigNat;

import java.awt.Dimension;
/**
 * Fenêtre d'information sur NAT
 * @author bruno
 *
 */
public class APropos extends JFrame implements ActionListener
{
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** Bouton fermant la fenêtre */
	private JButton fermer = new JButton("Fermer");
	/** JLabel de titre */
	private JLabel jlHead = new JLabel();
	/** JLabel contenant le texte à afficher (en pseudo-html pour java)*/
	private JLabel texte = new JLabel();
	/** largeur de la fenêtre */
	private int largeur = 580;
	/** hauteur de la fenêtre */
	private int hauteur = 600;
	/** JLabel pour le logo de nat */
	private JLabel logo = new JLabel();
	/** Constructeur */
	public APropos()
	{
		super("A propos de NAT");
		setLayout(new BorderLayout(5, 5));
		setSize(new Dimension(largeur,hauteur));
		jlHead.setIcon(new ImageIcon("ui/logoNat.png"));
		//jlHead.setText("<html><img src=\"file://" + Toolkit.getDefaultToolkit().natbraille.free.fr/images/logoNat.png\"></html>");
		setResizable(false);
		jlHead.setText("<html><center>"+
				"<h2>NAT "+ConfigNat.getVersionLong()+"</h2>" +
				"<p><small>Version SVN: </small>" + ConfigNat.getSvnVersion() +"</p><br>" +
				"<small><p>NAT est un logiciel libre. Lire le fichier licence.txt pour plus d'informations</p></small>" +
				"<br><p>Cette version de NAT a été financée par le Ministère de l'Education Nationale (service SDTICE) "+
				"et le Laboratoire d'InfoRmatique en Image et Systèmes d'information (LIRIS).</p>" +
				"</center></html>");
		//texte.insert("NAT est un logiciel libre créé dans le cadre du Master Handi de l'université Paris 8.",texte.getLineCount());
		texte.setText(
			"<html>" +
			"<p>Développements</p>" +
			"<ul><li>Bruno Mascret, Frédéric Schwebel, <small>équipe SILEX du LIRIS</small></li>"+
			"<li>Vivien Guillet, <small>Mission handicap de Lyon 1</small></li>" +
			"<li><small>Conception et réalisation initiales:</small> Bruno Mascret</li></ul>" +
			"<p>Eléments de programmation intégrés</p>" +
			"<ul><li>Frédéric Schwebel et al., projet BraMaNet, <small>Mission Handicap de Lyon 1</small></li>" +
			"<li>Henrik Just, <small>Writer2Latex</small></li>" +
			"<li>La communauté des développeurs d'OpenOffice</li>" +
			"<li>Paul R. Holser, JOptSimple <small>(options en ligne de commande)</small></li>" +
			"<li>Mickael Kay, Saxon B <small>(processeur xslt)</small></li>" + 
			"<li>Java Help <small>(moteur pour l'aide)</small></li>"+
			"<li>Rene Heuer, v2Math <small>(affichage du mathml, adaptation Bruno M.)</small></li> " +
			"<li>Mirko Nasato, JODConverter <small>(conversion des formats propriétaires en ODT)</small></li> " +
			"</ul>" +
			"<p>Testeurs</p>" +
			"<ul><li><small>Pilotage des tests: </small>Marc Ollier<small>, INS HEA (ex CNEFEI) de Suresnes</small></li>" +
			"<li><small>Braille littéraire:</small> Christiane Perdoux, Olga D'Amore</li>" +
			"<li><small>Braille mathématique:</small> Françoise Magna <small>(INJA)</small></li>" +
			"<li><small>Braille musical (en cours):</small> Marie-Claude Cressant, Marie-Andrée Courjault</li>"+
			"<li><small>Interfaces graphiques:</small> Christiane Perdoux</li>"+
			"<li><small>Accessibilité:</small> Olga D'amore</li>"+
			"</ul>" +
			"<p>Suivi du projet</p>" +
			"<ul><li>Alain Mille, <small>gestion et pilotage des développements, SILEX</small></li>" +
			"<li>Jack Sagot, <small>responsable du projet pour l'INS HEA</small></li>" +
			"<li>Thierry Bertrand<small>, responsable du projet au SDTICE (Ministère de l'éducation nationale)</small></li>" +
			"</ul>" +
			"<p>Contributions</p>" +
			"<ul><li>Raphaël Mina <small>(stage 3-if INSA), transcription inverse, conversion via jodconverter</small></li>" +
			"<li>Didier Erin <small>(stage LIRIS), initiation des développements pour le Braille musical</small></li>" +
			"<li>Benoît Dasset <small>(fonte Braille Antoine)</small>, Gérard Uzan <small>(ergonomie), </small>Bruno Blanchard<small> (v1.0)</small></li>" +
			"</ul>" +
			"<p>Soutien moral et logistique</p>" +
			//				"<img src=\"http://www.schneider-electric.com/gc_1_0/images/structure/signature.gif \">" +
			"<small><ul><li>Aude Lancelle et la famille Mascret</li>" +
			"<li>Claire, Hélène, Joachim, Julien, Manuella et Rémi</li>" +
			"</ul></small>" +
			"</html>");
		fermer.addActionListener(this);
		fermer.setPreferredSize(new Dimension(100,30));
		fermer.setSize(new Dimension(100,30));
		//fermer.getAccessibleContext().setAccessibleName("Fermer la fenêtre");
		//fermer.getAccessibleContext().setAccessibleDescription("Valider pour fermer la fenêtre");
		fermer.setToolTipText("Pour fermer la fenêtre (Alt+f)");
		fermer.setMnemonic('f');
		
		JScrollPane scrollRes = new JScrollPane (texte);
		scrollRes.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollRes.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollRes.setPreferredSize(new Dimension(largeur-40, hauteur - 200));
		texte.setBackground(Color.getHSBColor((float)0.0, (float)0.0, (float)10.0));
		texte.setOpaque(true);
		jlHead.setPreferredSize(new Dimension(largeur-40,190));
		JPanel pHead=new JPanel();
		pHead.setLayout(new BorderLayout());
		pHead.add("West",logo);
		pHead.add("Center",jlHead);
		add("North", pHead);
		add("Center",scrollRes);
		JPanel pFermer = new JPanel();
		pFermer.add(fermer);
		add("South",pFermer);
		if(ConfigNat.getCurrentConfig().getCentrerFenetre())
		{
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension size = this.getPreferredSize();
			screenSize.height = screenSize.height/2;
			screenSize.width = screenSize.width/2;
			size.height = size.height/2;
			size.width = size.width/2;
			int y = screenSize.height - size.height;
			int x = screenSize.width - size.width;
			setLocation(x, y);
		}
	}
	/**
	 * Implémentation de java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * <p>Gére la fermeture de la fenêtre</p>
	 * @see #fermer
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent evt){if (evt.getSource()==fermer){this.dispose();}}
}
